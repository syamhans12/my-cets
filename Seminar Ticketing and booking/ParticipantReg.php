<?php
   session_start();
   include "configDB.php"
?>

<html>
<head>
    <link rel="stylesheet" href="design.css"/>
    <link rel="stylesheet" href="custom-style.css">
    <title>Mycets-Motivation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="datepicker-style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
    </script>
  </head>
<div class="jumbotron">
    <div id="zero_box">
        <h1>MY CETS</h1>
        <p><i>Your number one choice!</i></p>
    </div>
<?php
  $programID = $_GET['id'];
  $sql = "SELECT * FROM registeredprogram WHERE id ='$_GET[id]'";
          
  $result = $conn->query($sql);
  while ($row = $result->fetch_assoc()){
    $date =new DateTime($row['lastsub']) ; 
    $now = new DateTime();

    if($date < $now){
      echo "<div class='row justify-content-md-center'>";
        echo "<div class='card bg-warning text-white w-50'>";
          echo "<h3>Last Submission Already Past</h3>";
        echo "</div>";
      echo "</div>";
        echo "<br>";
            echo "<div class='row justify-content-around'>";
               echo "<div class='card text-center '>";
                   echo "<button onclick=document.location='Seminar_homepage.php' class ='btn btn-primary'>Home Page</button>";
                echo "</div>";
            echo "</div>";
    }else{
      $username = $_SESSION["User"];
      $sql = "SELECT * FROM user WHERE Username = '$username'";
    
            $result = $conn->query($sql);
            while ($row = $result->fetch_assoc()){
              $id = $row['UserID'];
              $email = $row['Email'];
              $phone = $row['Phone']; 
            }

      $programID = $_GET['id'];      

      $sql = "SELECT * FROM registeredprogram WHERE id ='$_GET[id]'";
    
            $result = $conn->query($sql);
            while ($row = $result->fetch_assoc()){
              $programname = $row['ProgramName']; 
              $startdate = $row['startdate'];
              echo "<br>";
              
            }
?> 
        <div class="container-fluid ">
            <div class="row justify-content-md-center">
                <div class="card w-50" >
                    <form action="register.php" method="POST">
                      <br>
                          <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="progname" class = "text-uppercase" style="padding-left:20px">Program Name </label>
                            <input type="progname" name ="progname" value="<?php echo $programname ?>" class="form-control" required>
                          </div>
                          <div class="form-group" style="padding-left:20px; padding-right:20px;">
                            <label for="username" class = "text-uppercase" style="padding-left:20px">Username </label>
                            <input type="username" name ="username" value="<?php echo $username ?>" class="form-control" required>
                          </div>
                          <div class="form-group" style="padding-left:20px; padding-right:20px;">
                            <label for="email" class = "text-uppercase" style="padding-left:20px">Email </label>
                            <input type="email" name ="email" value="<?php echo $email ?>" class="form-control" required>
                          </div>
                          <div class="form-group" style="padding-left:20px; padding-right:20px;">
                            <label for="phone" class = "text-uppercase" style="padding-left:20px">Phone Number </label>
                            <input type="phone" name ="phone" value="<?php echo $phone ?>" class="form-control" required>
                          </div>  
                          <div class="form-group" style="padding-left:20px; padding-right:20px;">
                            <label for="startdate" class = "text-uppercase" style="padding-left:20px">Start date </label>
                            <input type="startdate" value="<?php echo $startdate ?>" class="form-control" required>
                          </div>
                          <div class="form-group" class = "text-uppercase" style="padding-left:20px">
                            <input type="hidden" name ="programID" value="<?php echo $programID ?>">
                            <input type="hidden" name ="userID" value="<?php echo $id ?>">
                          </div>
                          <div class="text-center">
                            <button type="submit" value="Confirm" class="btn btn-success"> Confirm </button>
                          </div>
                    </form>
                </div>
              </div>
        </div>
<?php
  }
  
}
?>
</html>