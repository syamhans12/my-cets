<?php
session_start();
include "configDB.php";
?>
<html>
<head>
    <link rel="stylesheet" href="design.css"/>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="custom-style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<BODY>
    <div class="jumbotron text-center">
        <div id="zero_box">
                <h1>MY CETS</h1>
                <p><i>Your number one choice!</i></p>
        </div>
    
        <table class= "table table-dark table-hover">
            <?php
                $search_value = $_SESSION["User"];
                $sql = "SELECT * FROM registeredprogram WHERE organizer = '$search_value'" ;
                $result = $conn->query($sql);

                if ($result->num_rows > 0) {
                    
                    echo "<tr>".
                    "<td><b>Program Name</b></td>".
                    "<td><b>Organizer</b></td>".
                    "<td><b>Email</b></td>".
                    "<td><b>About</b></td>".
                    "<td><b>Start Date</b></td>".
                    "<td><b>End Date</b></td>".
                    "<td><b>Last Submission Date</b></td>".
                    "</tr>";

                    $result = $conn->query($sql);
                    while ($row = $result->fetch_assoc()){

                        echo "<tr><td>".$row['ProgramName']."</td><td>". 
                        $row['organizer'] ."</td><td>".
                        $row['email'] ."</td><td>".
                        $row['description'] ."</td><td>".
                        $row['startdate']."</td><td>".
                        $row['enddate']."</td><td>".
                        $row['lastsub'].
                        "</td></tr>";
                        
                    } 
                } else {
                    echo "<h3>There Is no registered Program Under You</h3>";
                }
            ?>
        </table>
        <div class="row justify-content-md-center">
                <div class="card bg-info text-center text-white">
                    <button onclick="document.location='Seminar_homepage.php'" class ="btn btn-primary">Home Page</button>
                </div>
        </div>
    </div>
</BODY>
</html>
