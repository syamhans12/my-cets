<?php
  session_start();
  include "configDB.php";
?>
<!DOCTYPE html>
<html lang="en">
<head>
<title>MY CETS - SEMINAR </title>
    <link rel="stylesheet" href="design.css"/>
    <style>
        h3{
            border: 2px solid black inset;
            background-image: linear-gradient(to bottom right, teal , white);
            margin: 12px;  
            text-align: center;
            
            padding: 10px;  
        }
  </style>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="custom-style.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
  
    
    
</head>
<body>

<div class="jumbotron text-center">
        <div id="zero_box">
            <h1>MY CETS - SEMINAR</h1>
            <p><i>Your number one choice!</i></p>
        </div>
        <div class="row justify-content-md-center">
            <div class="card bg-info text-center text-white">
                <button class="but btn-primary" onclick= "location.href='/ProjectAD/mycets/MainLogin/mainpage.php'">MyCets Menu</button>
            </div>
        </div>
</div>




<div class="container mw-100">
  <div class="row">
    <div class="col-md-4">
      <div class="image-1">
      <a href="seminar.php" class="btn btn-info seminar btn-rounded stretched-link"> seminar &raquo;</a>
      </div>
      </div>
    <div class="col-md-4">
      <div class="image-2">
      <a href="workshop.php" class="btn btn-info workshop btn-rounded stretched-link"> workshop &raquo;</a>
    </div>
    </div>
    <div class="col-md-4">
      <div class="image-3">
      <a href="training.php" class="btn btn-info training btn-rounded stretched-link"> training &raquo;</a>
    </div>
    </div>
  </div>
  <br>
  <div class="row">
    <div class="col-md-4 offset-md-2">
      <div class="image-4">
      <a href="mentoring.php" class="btn btn-info mentoring btn-rounded stretched-link"> mentoring &raquo;</a>
    </div>
    </div>
    <div class="col-md-4">
      <div class="image-5">
      <a href="motivation.php" class="btn btn-info motivation btn-rounded stretched-link"> motivation &raquo;</a>
    </div>
  </div>
</div>
<br>
  <div class="row justify-content-around">
  <div class="card text-center">
    <a href="Insert_Program.php" class="col btn btn-info motivation btn-rounded"> Insert New Program &raquo;</a>
  </div>
  <div class="card text-center">
    <a href="ViewYourProgram.php" class="col btn btn-info motivation btn-rounded"> View Your Program &raquo;</a>
  </div>
  <div class="card text-center">
    <a href="ViewParticipant.php" class="col btn btn-info motivation btn-rounded"> View your participant &raquo;</a>
  </div>
  <div class="card text-center">
    <a href="ViewProgramJoin.php" class="col btn btn-info motivation btn-rounded"> View program you join &raquo;</a>
  </div>
</div>
<div class="card-body">
  <h4>UPCOMING EVENTS!!!</h4>

        <div>
        <table class="table table-dark table-hover">

                <?php
        
                        $sql = "SELECT * FROM registeredprogram ";
                        $result = $conn->query($sql);

                        if($result->num_rows > 0){

                            echo "<tr>".
                            "<td><b>Program Name</b></td>".
                            "<td><b>Category</b></td>".
                            "<td><b>Organizer</b></td>".
                            "<td><b>Email</b></td>".
                            "<td><b>About</b></td>".
                            "<td><b>Start Date</b></td>".
                            "</tr>";

                          while ($row = $result->fetch_assoc()){

                            echo "<tr><td>".$row['ProgramName']."</td><td>".
                            $row['category'] ."</td><td>".
                            $row['organizer'] ."</td><td>".
                            $row['email'] ."</td><td>".
                            $row['description'] ."</td><td>".
                            $row['startdate'].
                            "</td>";

                            echo
                            "<td><a href=ParticipantReg.php?id=".$row['id'].">Register Now</a></td></tr>";
                              
                          }
                        }

                ?>
            </table>
        </div>
  </div>



</div>

</body>
</html>
