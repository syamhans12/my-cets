<?php
namespace Phppot;

use \Phppot\Product;
require_once 'Product.php';

$product = new Product();
$productResult = $product->getAllProduct($programID);
$programID = $_POST["id"];
if (isset($_POST["export"])) {
    $product->exportProductDatabase($productResult);
}

require_once "./view/product-list.php";
?>
