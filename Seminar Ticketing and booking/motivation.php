<?php
  session_start();
  include "configDB.php"
?>
<!DOCTYPE html>
<html>
  <head>
    <title>Mycets-Motivation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="datepicker-style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>

  </head>
  <body>

    <div class="jumbotron text-center" style="background-image: url('image/motivation.png')";>
      <h1>Motivation</h1>
    </div>
    <div class="row">
      <div class="offset-md-5 md-form md-outline input-with-post-icon datepicker">
            <form action="" method="POST">
              <input placeholder="Select date" type="date" id="date" name="date" class="form-control">
              <label for="date"></label>
              <button type="submit" name= "search" onclick="dateinput()" class="offset-md-5 btn btn-info">search</button>
            </form>
      </div>
    </div>
    <div class="card-body">
        <table class= "table table-dark table-hover">

                <?php
                  if (isset($_POST['search'])) {
                      $search_value=$_POST["date"];
                        $sql = "SELECT * FROM registeredprogram WHERE startdate = '$search_value' AND category = 'motivation'" ;
                
                        echo "<tr>".
                        "<td><b>Program Name</b></td>".
                        "<td><b>Organizer</b></td>".
                        "<td><b>Email</b></td>".
                        "<td><b>About</b></td>".
                        "<td><b>Start Date</b></td>".
                        "</tr>";

                        $result = $conn->query($sql);
                        while ($row = $result->fetch_assoc()){

                          echo "<tr><td>".$row['ProgramName']."</td><td>". 
                          $row['organizer'] ."</td><td>".
                          $row['email'] ."</td><td>".
                          $row['description'] ."</td><td>".
                          $row['startdate'].
                          "</td>";

                          echo "<td><button class='button' style='vertical-align:middle'><span>";
                          echo
                          "<a href=ParticipantReg.php?id=".$row['id'].">Register Now</a></span></button></td></tr>";
                            
                        }

                          
                    }
                ?>
          </table>
        </div>
        <h4>UPCOMING EVENTS!!!</h4>

        <div>
        <table class="table table-dark table-hover">

                <?php
        
                        $sql = "SELECT * FROM registeredprogram WHERE category = 'motivation'";
                        $result = $conn->query($sql);

                        if($result->num_rows > 0){

                            echo "<tr>".
                            "<td><b>Program Name</b></td>".
                            "<td><b>Category</b></td>".
                            "<td><b>Organizer</b></td>".
                            "<td><b>Email</b></td>".
                            "<td><b>About</b></td>".
                            "<td><b>Start Date</b></td>".
                            "</tr>";

                          while ($row = $result->fetch_assoc()){

                            echo "<tr><td>".$row['ProgramName']."</td><td>".
                            $row['category'] ."</td><td>".
                            $row['organizer'] ."</td><td>".
                            $row['email'] ."</td><td>".
                            $row['description'] ."</td><td>".
                            $row['startdate'].
                            "</td>";

                            echo
                            "<td><a href=ParticipantReg.php?id=".$row['id'].">Register Now</a></td></tr>";
                              
                          }
                        }

                ?>
            </table>
        </div>
  </div>
  </body>
</html>