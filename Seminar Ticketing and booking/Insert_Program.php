<?php 
    session_start();
    include "configDB.php"
?>
<html> 
<head>
    <link rel="stylesheet" href="design.css"/>
    <link rel="stylesheet" href="custom-style.css">
    <title>Mycets-Motivation</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="datepicker-style.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
    <script>
        // Disable form submissions if there are invalid fields
        (function() {
        'use strict';
        window.addEventListener('load', function() {
            // Get the forms we want to add validation styles to
            var forms = document.getElementsByClassName('needs-validation');
            // Loop over them and prevent submission
            var validation = Array.prototype.filter.call(forms, function(form) {
            form.addEventListener('submit', function(event) {
                if (form.checkValidity() === false) {
                event.preventDefault();
                event.stopPropagation();
                }
                form.classList.add('was-validated');
            }, false);
            });
        }, false);
        })();
    </script>
</head>
<body>
    <div class="jumbotron">
        <div id="zero_box">
            <h1>MY CETS</h1>
            <p><i>Your number one choice!</i></p>
        </div>
        <div class="container-fluid ">
            <div class="row justify-content-md-center">
                <div class="card w-50" >
                    <form action="RegisterProgram.php" method="post">
                        <br>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="ProgramName">Program Name: </label>
                            <input type="text" id="PName" name="ProgramName" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="organizer">Organizer: </label>
                            <input type="text" id="organizer" name="organizer" value ="<?php echo $_SESSION['User']?>" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="ProgramDesc">Program Description: </label>
                            <input type="text" id="desc" name="description" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="category">Choose a Category of your program:</label>
                            <select name="category" id="category" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                                <option value="seminar">Seminar</option>
                                <option value="mentoring">Mentoring</option>
                                <option value="workshop">Workshop</option>
                                <option value="training">traning</option>
                                <option value="motivation">Motivation</option>
                            </select>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="quantity">Quantity of Participant:</label>
                            <input type="number" id="quantity" name="quantity" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="email">Enter organizer email:</label>
                            <input type="email" id="email" name="email" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="startdate">Start Date:</label>
                            <input type="date" id="startdate" name="startdate" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="enddate">End Date:</label>
                            <input type="date" id="enddate" name="enddate" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="form-group " style="padding-left:20px; padding-right:20px;">
                            <label for="lastsub">Last Entry Submission:</label>
                            <input type="date" id="lastsub" name="lastsub" class="form-control" required>
                            <div class="valid-feedback">Valid.</div>
                            <div class="invalid-feedback">Please fill out this field.</div>
                        </div>
                        <div class="text-center">
                            <button type="submit" value="Confirm" class="btn btn-success"> Confirm </button>
                        </div> 
                    </form>
                    <div class="row justify-content-md-center">
                        <div class="card text-center ">
                            <button onclick="document.location='Seminar_homepage.php'" class ="btn btn-danger">Cancel</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</body>
</html>