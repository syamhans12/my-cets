<!DOCTYPE html>

<html>

   <head>
    <title>Payment Done</title>

   <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">
    <style>
        body {background-color: #84bad9;}

        div{
            background-color: lightgrey;
            width: 400px;
            padding: 50px 100px 100px 100px;
            margin-top: 100px;
            margin-left:auto;
            margin-right:auto;
            }

        img{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
        }

        button {
                margin-top:100px;
                margin-right:50px;
                margin-left:50px;
                padding: 15px;

                }
    </style>

   </head>

   <body>

       <div>
            <p> Thanks for the payment, please check your email for the receipt</p>
            <button class="button btn-lg" onclick= "location.href='index.php'">back</button>
       </div>
   </body>
</html>
