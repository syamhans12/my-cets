<!DOCTYPE html>

<html>

   <head>

    <link rel="stylesheet" href="design.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <title>MYCETS Ticketing System</title>

	<meta charset = "utf-8">
	<meta http-equiv = "X-UA-Compatible" content = "IE=edge">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">

	<link href = "https://fonts.googleapis.com/css?family=PT+Sans:400" rel = "stylesheet">
	<link type = "text/css" rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/busTicketing.js"></script>
    </head>
    <style>

        body{
            background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
            background-size: 400% 400%;
            animation: gradient 15s ease infinite;
            }

        @keyframes gradient
        {
        0%   { background-position: 0% 50%;   }
        50%  { background-position: 100% 50%; }
        100% { background-position: 0% 50%;   }
        }

        @keyframes divMOVE
        {
            0%   { background-position: 0% 50%;   }
            50%  { background-position: 100% 50%; }
            100% { background-position: 0% 50%;   }
        }


        div{
            background-color:linear-gradient(-45deg,#23d5ab,#ee7752,#e73c7e,#23a6d5);
            width: 400px;
            padding: 50px 100px 100px 100px;
            margin-top: 100px;
            margin-left:auto;
            margin-right:auto;
            border-radius: 15px 50px 30px;
            animation: divMOVE 15s ease infinite;
            }

        img{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
        }

        button {
                padding: 25px 55px;
                font-size: 24px;
                text-align: center;
                cursor: pointer;
                outline: #00ff00;
                color: #fff;
                background-color:#23d5ab;
                border: #009900;
                border-radius: 15px;
                box-shadow: 0 9px #999;



                margin-top:100px;
                margin-right:50px;
                margin-left:50px;
                padding: 15px;
                }
                .button:hover {background-color: #3e8e41}

                .button:active {
                background-color: #009900;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
                }



}
    </style>



   <body>
        <div id="zero_box">
            <h1>MY CETS - Centeralize Ticketing System</h1>
            <p><i>Your number one choice!</i></p>

            <img src="park1.PNG" alt="Parking Lot" >
            <button class="button btn-lg" onclick= "location.href='index2.php'">Let's Park</button>
            <button class="button btn-lg" onclick= "location.href='Login.php'">Admin Page</button>
            <button class="button btn-lg" onclick= "location.href='/ProjectAD/mycets/MainLogin/mainpage.php'">My Cets Menu</button>
        </div>

       <div>

       </div>
   </body>
</html>
