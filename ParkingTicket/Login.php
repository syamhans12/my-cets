<!DOCTYPE html>
<?php include "connection.php"; ob_start();?>
<?php session_start(); ?>

<html>

   <head>

    <link rel="stylesheet" href="design.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://www.w3schools.com/w3css/4/w3.css">

    <title>MYCETS Ticketing System</title>

	<meta charset = "utf-8">
	<meta http-equiv = "X-UA-Compatible" content = "IE=edge">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">

	<link href = "https://fonts.googleapis.com/css?family=PT+Sans:400" rel = "stylesheet">
	<link type = "text/css" rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/busTicketing.js"></script>
    </head>
    <style>

        body{
            background: linear-gradient(-45deg, #ee7752, #e73c7e, #23a6d5, #23d5ab);
            background-size: 400% 400%;
            animation: gradient 15s ease infinite;
            }

        @keyframes gradient
        {
        0%   { background-position: 0% 50%;   }
        50%  { background-position: 100% 50%; }
        100% { background-position: 0% 50%;   }
        }

        @keyframes divMOVE
        {
            0%   { background-position: 0% 50%;   }
            50%  { background-position: 100% 50%; }
            100% { background-position: 0% 50%;   }
        }


        div{
            background-color:linear-gradient(-45deg,#23d5ab,#ee7752,#e73c7e,#23a6d5);
            width: 400px;
            padding: 50px 100px 100px 100px;
            margin-top: 100px;
            margin-left:auto;
            margin-right:auto;
            border-radius: 15px 50px 30px;
            animation: divMOVE 15s ease infinite;
            }

        img{
            display: block;
            margin-left: auto;
            margin-right: auto;
            width: 100%;
        }

        button {
                padding: 25px 55px;
                font-size: 24px;
                text-align: center;
                cursor: pointer;
                outline: #00ff00;
                color: black;
                background-color:#23d5ab;
                border: #009900;
                border-radius: 15px;
                box-shadow: 0 9px #999;



                margin-top:100px;
                margin-right:50px;
                margin-left:50px;
                padding: 15px;
                }
                .button:hover {background-color: #3e8e41}

                .button:active {
                background-color: #009900;
                box-shadow: 0 5px #666;
                transform: translateY(4px);
                }



}
    </style>



   <body>
         <form class="loginform-content animate" action="login.php" method="post">
           <div id="zero_box">
               <h1>MY CETS - Centeralize Ticketing System</h1>
               <p><i>Your number one choice!</i></p>



             <label for="uname"><b>Username</b></label>
             <input type="text" placeholder="Enter Username" name="uname" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>" required>

             <label for="psw"><b>Password</b></label>
             <input type="password" placeholder="Enter Password" name="psw" value="<?php if(isset($_COOKIE["pass"])) { echo $_COOKIE["pass"]; } ?>" required>

             <button type="submit" name="login">Login</button>


        </form>




   <?php

if (isset($_POST['login'])) {

	$username = $_POST['uname']; //login username value
	$password = $_POST['psw']; //login password value

	$query = "SELECT * FROM user WHERE Username = '$username'"; //select from user table where username is same with
	$selected_user = mysqli_query($db_handle,$query); //search in database

	if (!$selected_user) { //if the username not found in database
		die("Query Failed" . mysqli_error($db_handle));
		print '<p>Wrong username!</p>';
		header("Location: login.php");
	}

	while ($row = mysqli_fetch_assoc($selected_user)) {
		$db_user_id = $row['UserID']; //store all user info
		$db_user_fname = $row['Fname'];
		$db_user_lname = $row['Lname'];
		$db_user_email= $row['Email'];
		$db_username = $row['Username'];
		$db_user_password = $row['Password'];
		$db_user_type = $row['UserType'];

		if($username === $db_username && $password === $db_user_password) {

			$_SESSION['id'] = $db_user_id; //store all user info to session
			$_SESSION['firstname'] = $db_user_firstname;
			$_SESSION['lastname'] = $db_user_lastname;
			$_SESSION['email'] = $db_user_email;
			$_SESSION['username'] = $db_username;
			$_SESSION['password'] = $db_user_password;
			$_SESSION['usertype'] = $db_user_type;

			//set cookie for 7 days
			if(isset($_POST['remember'])) {
				setcookie('username', $username, time()+60*60*7);
				setcookie('pass', $password, time()+60*60*7);
			}

			if ($db_user_type === 'admin') { //if role is admin, enter admin interface
				print '<p>Admin!</p>';
				header("Location: AdminMainPage.php");
				exit;
			}
			else if ($db_user_type === 'normal_user') { //if role is normal user, enter user interface
				print '<p>User!</p>';
				header("Location: index.php");
				exit;
			}
		}

		else { //if the password not match
			print '<p>Wrong password!</p>';
			header("Location: login.php");
			exit;
		}
	}
}

?>
   </body>
</html>
