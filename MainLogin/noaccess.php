<html>
<head>
    <title>MY CETS - No access</title>
    <link rel="stylesheet" href="design.css"/>
</head>

<body>

    <div id="container">
        <div id="zero_box">
            <h1>MY CETS - Centeralize Ticketing System</h1>
            <p><i>Your number one choice!</i></p>
        </div>

        <div id="first_box">
        <?php
                if(!isset($_SESSION))
                {
                   session_start();
                }
                if($_SESSION["Login"]!= "YES")
                    header("location:user_login.php");

                if (isset($_SESSION['User']))
                {
                        echo "<p>| User ID: ".$_SESSION["ID"];
                        echo "| Current user: ".$_SESSION['User'];
                        echo '<a href="logout.php?logout"> | Logout | </a>';
                }
                else
                {
                    header("location:user_login.php");
                }
                
            ?>

        </div>

        <div class="second_box">
            <div>
            <h3>You have no access.</h3>
            <p>Please choose one of option below.<p>
            </div>
        </div>
        <div class="third_box">
        <div style="cursor:pointer;" onclick="window.location.href='mainpage.php';"><u>Back to main menu.</u></div>
        <div style="cursor:pointer;" onclick="window.location.href='/ProjectAD/mycets/Football&Concert/CO-Page1.php';"><u>Concert Main Page.</u></div>
        <div style="cursor:pointer;" onclick="window.location.href='/ProjectAD/mycets/Football&Concert/FB-Page1.php';"><u>Football Main Page.</u></div>
        </div>
        <div class="fourth_box">
            
        </div>
    </div>

</body>


</html>