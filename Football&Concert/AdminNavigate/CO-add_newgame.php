<!DOCTYPE html>
<html>
<head>
    <title>Concert-Add New Concert</title>
    <link rel="stylesheet" href="/ProjectAD/mycets/Football&Concert/design_2.css"/>
    <script>
    function validate()
            {
                if(document.addform.countryName.value == "")
                {
                    alert("Please provide the country name!");
                    document.addform.countryName.focus();
                    return false;
                }

                if(document.addform.artistName.value == "")
                {
                    alert("Please provide the name of the artist!");
                    document.addform.artistName.focus();
                    return false;
                }

                if(document.addform.timeDate.value == "")
                {
                    alert("Please provide the date and time of the match!");
                    document.addform.timeDate.focus();
                    return false;
                }   
            }

    </script>
    
    <style>
        h3{
            border: 2px solid black inset;
            background-image: linear-gradient(to bottom right, teal , white);

            margin: 12px;  
            text-align: center;
            border-radius: 15px;
            padding: 10px;  
        }
    </style>
    
</head>
<body>
<div id="container">
        
        <div id="zero_box">
            <h1>MY CETS - CONCERT TICKET</h1>
            <p><i>Your number one choice!</i></p>
        </div>


        <div class="zero_box_1">
            <div>
                <?php    
                    session_start();   
                    if($_SESSION["Login"]!= "YES")
                        header("location:user_login.php");

                    if (isset($_SESSION['User']))
                    {
                        echo "<p style='color:white;'>| User ID: ".$_SESSION["ID"];
                        echo "| Current user: ".$_SESSION['User'];

                            
                    }
                    else
                    {
                        header("location:/ProjectAD/mycets/MainLogin/user_login.php");
                    }
                    
                ?>
                
            </div>
            <div>
                <a href="/ProjectAD/mycets/MainLogin/logout.php?logout"> | Logout </a>
                <a href="/ProjectAD/mycets/Football&Concert/CO-admin.php?"> | Concert Admin Menu </a>
                <p>
                <?php
                
                if(isset($_POST['insert']))
                {
                    $countryName = $_POST['countryName'];
                    $artistName = $_POST['artistName'];
                    $timeDate = $_POST['timeDate'];
                    require_once("config.php");
                    $query= " INSERT INTO concert (country_name,concert_name,time_date)
                    VALUES ('$countryName','$artistName','$timeDate')";
                    

                    $result=mysqli_query($conn,$query);

                    if($result)
                    {
                        echo '<p>Succesfully added new concert</p>';

                    }
                        
                    else
                    {
                        echo '<p>Adding concert failed.</p>';
                    }

                    mysqli_close($conn);
                }
                
                ?>
                </p>
            </div>
            
        </div>
        <div id="second_box">
            <h3>Add New Concert</h3>
            <div class="container2">
            <table>
            <form name="addform" action="/ProjectAD/mycets/Football&Concert/AdminNavigate/CO-add_newgame.php" method="POST" onsubmit="return(validate());">
                <tr>
                <td><label for="countryName">Country Name :</label><br></td>
                <td><input  type="text" name="countryName"><br></td>
                </tr>
                <tr>
                <td><label for="artistName">Artist Perform:</label><br></td>
                <td><input  type="text" name="artistName"><br></td>
                </tr>
                <tr>
                <td><label for="timeDate">Date & Time (yyyy-mm-dd hh:mm:ss):</label><br></td>
                <td><input  type="datetime-local" name="timeDate"><br></td>
                </tr>
                <tr>
                <td></td>
                <td colspan="2"><input type="submit" name="insert" value="Add new concert"></td>
                </tr>
            </form>
            </table>    
            </div>
            
        </div>
       
        
</div>
</body>

    
</html>

