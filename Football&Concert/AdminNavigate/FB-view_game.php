<!DOCTYPE html>
<html>
<head>
    <title>Football-View All Game</title>
    <link rel="stylesheet" href="/ProjectAD/mycets/Football&Concert/design.css"/>
    
    <style>
        h3{
            border: 2px solid black inset;
            background-image: linear-gradient(to bottom right, teal , white);

            margin: 12px;  
            text-align: center;
            border-radius: 15px;
            padding: 10px;  
        }
    </style>
    
</head>
<body>
<div id="container">
        
        <div id="zero_box">
            <h1>MY CETS - FOOTBALL TICKET</h1>
            <p><i>Your number one choice!</i></p>
        </div>


        <div class="zero_box_1">
            <div>
                <?php    
                    session_start();   
                    if($_SESSION["Login"]!= "YES")
                        header("location:user_login.php");

                    if (isset($_SESSION['User']))
                    {
                        echo "<p style='color:white;'>| User ID: ".$_SESSION["ID"];
                        echo "| Current user: ".$_SESSION['User'];

                            
                    }
                    else
                    {
                        header("location:/ProjectAD/mycets/MainLogin/user_login.php");
                    }
                    
                ?>
                
            </div>
            <div>
                <a href="/ProjectAD/mycets/MainLogin/logout.php?logout"> | Logout </a>
                <a href="/ProjectAD/mycets/Football&Concert/FB-admin.php?"> | Football Admin Menu </a>
                <p><?php
                if(@$_GET['success']==true)
                    {
                ?>
                    <div><?php echo $_GET["success"]?></div>
                <?php
                    }
                ?></p>
            </div>
            
        </div>
        <div id="second_box">
            <h3>View All Game</h3>
            <div class="container2">
            <table>
                <tr>
                    <th>Country Name</th>
                    <th>Match Name</th>
                    <th>Time & Date</th>
                </tr>
                <?php
                require_once("config.php");
                $sql= "SELECT * FROM football";
                $records = mysqli_query($conn,$sql);

                while($row =mysqli_fetch_array($records))
                {
                    echo "<tr><td>".$row['country_name']."</td><td>".
                    $row['match_name']."</td><td>".
                    $row['time_date']."</td><td><a href=FB-delete.php?id=".
                    $row['id'].">Delete</a></td></tr>";
                }

                ?>

            </table>
            
            </div>
            
        </div>
       
        
</div>
</body>

    
</html>

