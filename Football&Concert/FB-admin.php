<!DOCTYPE html>
<html>
<head>
    <title>Football-Admin</title>
    <link rel="stylesheet" href="design.css"/>
    <style>
        h3{
            border: 2px solid black inset;
            background-image: linear-gradient(to bottom right, teal , white);

            margin: 12px;  
            text-align: center;
            border-radius: 15px;
            padding: 10px;  
        }
    </style>
    
</head>
<body>
<div id="container">
        
        <div id="zero_box">
            <h1>MY CETS - FOOTBALL TICKET</h1>
            <p><i>Your number one choice!</i></p>
        </div>


        <div class="zero_box_1">
            <div>
            <?php
                        session_start();
                        
                    if($_SESSION["Login"]!= "YES")
                        header("location:user_login.php");

                    if (isset($_SESSION['User']))
                    {
                        echo "<p style='color:white;'>| User ID: ".$_SESSION["ID"];
                        echo "| Current user: ".$_SESSION['User'];

                            
                    }
                    else
                    {
                        header("location:/ProjectAD/mycets/MainLogin/user_login.php");
                    }
                    
                ?>
            </div>
            <div>
                <a href="/ProjectAD/mycets/MainLogin/logout.php?logout"> | Logout </a>
                <a href="/ProjectAD/mycets/MainLogin/admin.php?admin"> | Admin Menu |</a></p>
            </div>
            
        </div>
        <div id="second_box">
            <div class="container2">
                <table>
                <tr>
                    <td><p>1.</p></td>
                    <td><a href="/ProjectAD/mycets/Football&Concert/AdminNavigate/FB-add_newgame.php">Add New Game</a></td>
                </tr>
                <tr>                            
                    <td>2.</td>
                    <td><a href="/ProjectAD/mycets/Football&Concert/AdminNavigate/FB-view_game.php">View all game</a></td>
                </tr>
                <tr>
                    <td>3.</td>
                    <td><a href="/ProjectAD/mycets/MainLogin/view_user.php">View all user</a></td>
                </tr>
                </table>
            </div>
            
        </div>
       
        
</div>
</body>

    
</html>

