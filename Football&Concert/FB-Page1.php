<!DOCTYPE html>
<html>
<head>
    <title>MY CETS - Football </title>
    <link rel="stylesheet" href="design.css"/>
    <style>
        h3{
            border: 2px solid black inset;
            background-image: linear-gradient(to bottom right, teal , white);
            margin: 12px;  
            text-align: center;
            
            padding: 10px;  
        }
    </style>
    
</head>
    <body>
    <div id="container">
        
        <div id="zero_box">
            <h1>MY CETS - FOOTBALL TICKET</h1>
            <p><i>Your number one choice!</i></p>
        </div>


        <div class="zero_box_1">
        <div>
        <?php
                    session_start();
                    
                if($_SESSION["Login"]!= "YES")
                    header("location:user_login.php");

                if (isset($_SESSION['User']))
                {
                    echo "<p style='color:white;'>| User ID: ".$_SESSION["ID"];
                    echo "| Current user: ".$_SESSION['User'];

                        
                }
                else
                {
                    header("location:/ProjectAD/mycets/MainLogin/user_login.php");
                }
                
            ?>
        </div>
        <div>
            <a href="/ProjectAD/mycets/MainLogin/logout.php?logout"> | Logout </a>

            <a href="/ProjectAD/mycets/MainLogin/mainpage.php"> | My-Cets Menu </a>

            <a href="/ProjectAD/mycets/MainLogin/admin.php?admin"> | Admin Only |</a></p>
        </div>
            
        </div>



        <div id="first_box">
        
        <form action="FB-Page2.php" method="POST">
        <table class="table">
            <tr>
            <td colspan="2"><h3>Find Your Tickets</h3></td>
            </tr>
            <tr>
            <th><label class="time_date">Date & Time : </label></th>
            <th><input type="datetime-local" id="time_date" name="time_date"></th>
            </tr>
            

            <tr>
            <th><label class="country">Country :</label></th>
            <th><select id="country" name="country_name">
                <option value="Kuala Lumpur">Kuala Lumpur</option>
                <option value="Labuan">Labuan</option>
                <option value="Johor">Johor</option>
                <option value="Kedah">Kedah</option>
                <option value="Kelantan">Kelantan</option>
                <option value="Melaka">Melaka</option>
                <option value="Negeri Sembilan">Negeri Sembilan</option>
                <option value="Pahang">Pahang</option>
                <option value="Perak">Perak</option>
                <option value="Perlis">Perlis</option>
                <option value="Penang">Penang</option>
                <option value="Sabah">Sabah</option>
                <option value="Sarawak">Sarawak</option>
                <option value="Selangor">Selangor</option>
                <option value="Terengganu">Terengganu</option>
                <option value="United Kingdom">United Kingdom</option>
                <option value="Spain">Spain</option>
                
             </select></th>
            </tr>

            <tr>
            <td></td>
            <td colspan="2"><input type="submit" name="search" value="Search Information"></td>
            </tr>
        </table>
        </form>

        <h3><a href="FB-purchasehistory.php">Purchase History</a></h3>

       



        </div>
        <div id="second_box">
            <h3>Upcoming Football Events</h3>
            <div class="container2">
                <table>
                    <tr>
                        <th>Country</th>
                        <th>Match</th>
                        <th>Date & Time</th>
                        <th>Action</th>
                    </tr>
                    <?php
                    require_once("config.php");
                    $sql= "SELECT * FROM football";
                    $records = mysqli_query($conn,$sql);
    
                    while($row =mysqli_fetch_array($records))
                    {
                        echo "<tr><td>".$row['country_name']."</td><td>".
                        $row['match_name']."</td><td>".
                        $row['time_date']?>
                        
                        </td><td><button class="button" style="vertical-align:middle"><span>
                        <?php
                        echo
                        "<a href=FB-ticketlist.php?id=".$row['id'].">Buy Now</a></span></button></td></tr>";
                    }
    
                    ?>
    
                </table>
            </div>
        </div>

       


    </div>
    </body>

    
</html>

