<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Paypal Payment</title>
    <link rel="stylesheet" href="style.css">
</head>
<body>
    <main id="cart-main">
        <div class="site-title text-center">
            <h1 class="font-title">Complete Payment Transaction</h1>
        </div>
        <div class="container">
            <div class="grid">
                <div class="col-1">
                    <div class="flex item justify-content-between">
                        <div class="flex">
                            <div class="img text-center">
                                <img src="./assets/pro1.png" alt="">
                            </div>
                            <div class="title">
                            <table>
                            <tr>
                                        <td>
                                        
                                                    <?php
                                                          
                                                               session_start();
                                                           
                                                        if($_SESSION["Login"]!= "YES")
                                                            header("location:user_login.php");

                                                        if (isset($_SESSION['User']))
                                                        {
                                                                echo "| User ID: ".$_SESSION["ID"];
                                                                echo "| Current user: ".$_SESSION['User'];
                                                    
                                                        }
                                                        else
                                                        {
                                                            header("location:/ProjectAD/mycets/MainLogin/user_login.php");
                                                        }
                                                        
                                                    ?>
        
                                        </td>
                                    </tr>
                                    <tr>
                                        <td><h3> Ticket Information </h3></td> 

                                    </tr>
                                    <?php
                                    
                                    require_once("config.php");
                                    $sql = "SELECT * FROM ticketfootball INNER JOIN football ON football.id = ticketfootball.match_id WHERE ticket_id ='$_GET[id]'";
                                    $records = mysqli_query($conn,$sql);
                                    while($row =mysqli_fetch_array($records))
                                    {
                                        echo "<tr><td>Ticket ID : </td><td>".$row['ticket_id']."</td></tr><tr><td>Ticket Category : </td><td>".
                                        $row['ticket_category']."</td></tr>
                                        <tr><td>Price Ticket : </td><td>"."RM ".
                                        $row['ticket_price']."</td></tr>
                                        <tr><td>Match Between :</td><td>".
                                        $row['match_name']."</td></tr><tr><td>Location :</td><td>".
                                        $row['country_name']."</td></tr><tr><td>Date & Time :</td><td>".
                                        $row['time_date']."</td></tr>";

                                    }

                                    
                                    
                                    ?>
                            </table>
                            </div>
                        </div>

                    </div>
                </div>
                <div class="col-2">
                    <div class="subtotal text-center">
                        <h3>Final Price Details</h3>
                        
                        <?php
                         
                         require_once("config.php");
                         $sql = "SELECT ticket_price,ticket_id FROM ticketfootball WHERE ticket_id ='$_GET[id]'";
                         
                         $records = mysqli_query($conn,$sql);
                         $rows=mysqli_fetch_assoc($records);
                         $ticket_id=$rows["ticket_id"];
                         $ticket_price=$rows["ticket_price"];
                         $count=mysqli_num_rows($records);
                         if($count==1)
                         {
                    
                                echo "Amount to pay : RM ".$ticket_price;
                               
    
                             $_SESSION['ticket_id']=$ticket_id;
                             header("window.location.replace:FB-success.php");
                         }


                        
                        ?>
                        

                        
                        <div id="paypal-payment-button">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </main>
    <?php
    
    ?>
    <script src="https://www.paypal.com/sdk/js?client-id=AVr_dg-3RyofTQDlk-Ejd0-Nt2D6cnKbBmXfquREj1nuok5hQDpTBrdpYew5fVt1u9iskGqCAeTgjXsX&disable-funding=credit,card"></script>
    <script>


    paypal.Buttons(
        
    {style : {color: 'blue',shape: 'pill'},
    
    createOrder: function (data, actions) {return actions.order.create({purchase_units : [{amount: {value: '0.1'}}]});},
    
    onApprove: function (data, actions) 
    {
        return actions.order.capture().then(function (details) 
        {
            
            console.log(details)
            
            window.location.replace("FB-success.php")
            
        })
    },

    onCancel: function (data) 
    {
        window.location.replace("FB-Oncancel.php")
    }
    }).render('#paypal-payment-button');
    
    </script>
</body>
</html>