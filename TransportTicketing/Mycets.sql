-- phpMyAdmin SQL Dump
-- version 5.0.2
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 07, 2020 at 02:15 PM
-- Server version: 10.4.13-MariaDB
-- PHP Version: 7.4.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `busonlineticketingsystem`
--
CREATE DATABASE IF NOT EXISTS `Mycets`;
-- --------------------------------------------------------

--
-- Table structure for table `booking`
--

CREATE TABLE `booking` (
  `Booking_id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Bus_id` int(11) NOT NULL,
  `User_id` int(11) DEFAULT NULL,
  `Seats_no` int(11) NOT NULL,
  `Total_fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `booking` (`Booking_id`, `user`, `Date`, `Bus_id`, `User_id`, `Seats_no`, `Total_fare`) VALUES
(1, 'Siti', '2020-06-03 23:34:38', 1, 1, 2, 55),
(2, 'Alice', '2020-06-04 16:03:02', 2, 2, 1, 55),
(3, 'Ali', '2020-06-04 18:52:06', 3, 3, 3, 60),
(4, 'alison', '2020-07-06 11:38:26', 4, 5, 1, 55),
(5, 'Wilson', '2020-07-06 11:39:58', 4, 6, 2, 120),
(6, 'Divya', '2020-06-03 13:34:38', 8, 7, 2, 110),
(7, 'QuanRine', '2020-06-04 09:03:02', 8, 8, 1, 55),
(8, 'Daniel', '2020-06-04 02:52:06', 3, 9, 1, 60),
(9, 'Zarifah', '2020-07-06 01:38:26', 1, 10, 1, 55),
(10, 'Yeoh', '2020-07-06 21:39:58', 26, 11, 2, 55),
(11, 'Tariq', '2020-06-03 21:34:38', 34, 12, 2, 50),
(12, 'Rooter', '2020-06-04 15:03:02', 40, 13, 1, 50),
(13, 'Candy', '2020-06-04 12:52:06', 12, 14, 1, 55),
(14, 'Horse', '2020-07-06 19:38:26', 20, 15, 1, 50),
(15, 'Kelvin', '2020-07-06 17:39:58', 24, 16, 2, 60),
(16, 'Siti', '2020-06-03 23:50:38', 21, 1, 2, 55),
(17, 'Alice', '2020-06-04 16:10:02', 22, 2, 1, 55),
(18, 'QuanRine', '2020-06-04 09:12:06', 29, 8, 1, 55),
(19, 'Daniel', '2020-07-06 22:38:26', 23, 9, 1, 55),
(20, 'Zarifah', '2020-07-06 01:39:58', 21, 10, 6, 55);

-- --------------------------------------------------------

--
-- Table structure for table `bus`
--

CREATE TABLE `bus` (
  `Trip_Id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Origin` varchar(200) NOT NULL,
  `Destination` varchar(200) NOT NULL,
  `Seats` int(11) NOT NULL,
  `Departure_time` time NOT NULL,
  `Date_` date NOT NULL,
  `Fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `bus`
--

INSERT INTO `bus` (`Trip_Id`, `Name`, `Origin`, `Destination`, `Seats`, `Departure_time`, `Date_`, `Fare`) VALUES
(1, 'Transnasional', 'Stesen Bas Jitra', 'Shah Alam', 7, '10:30:00', '2021-02-14', 55),
(2, 'Transnasional', 'Stesen Bas Jitra', 'Ipoh', 18, '10:00:00', '2021-02-14', 55),
(3, 'Transnational', 'Stesen Bas Jitra', 'Ipoh', 18, '15:00:00', '2021-02-14', 55),
(4, 'Transnational', 'Stesen Bas Jitra', 'Butterwoth', 20, '10:30:00', '2021-02-14', 60),
(5, 'Transnatinal', 'Stesen Bas Jitra', 'Butterwoth', 11, '16:00:00', '2021-02-14', 60),
(6, 'Sani Express', 'TBS', 'Batu Pahat', 35, '09:00:00', '2021-02-14', 55),
(7, 'Sani Express', 'TBS', 'Batu Pahat', 7, '13:00:00', '2021-02-14', 55),
(8, 'Sani Express', 'TBS', 'Kulai', 18, '10:00:00', '2021-02-14', 55),
(9, 'Sani Express', 'TBS', 'Kulai', 20, '18:00:00', '2021-02-14', 55),
(10, 'Sani Express', 'TBS', 'Melaka', 11, '12:00:00', '2021-02-14', 50),
(11, 'Queen Express', 'Kuatan', 'Kuala Lumpur', 35, '11:30:00', '2021-02-14', 55),
(12, 'Queen Express', 'Kuantan', 'Kuala Lumpur', 7, '16:30:00', '2021-02-14', 55),
(13, 'Queen Express', 'Raub', 'Kuantan', 18, '08:30:00', '2021-02-14', 55),
(14, 'Queen Express', 'Raub', 'Kuantan', 20, '14:30:00', '2021-02-14', 50),
(15, 'Queen Express', 'Kuantan', 'Johor Bahru', 11, '12:00:00', '2021-02-14', 50),
(16, 'Perdana Express', 'Ipoh', 'Alor Setar', 35, '09:00:00', '2021-02-14', 55),
(17, 'Perdana Express', 'Ipoh', 'Teluk Intan', 7, '11:15:00', '2021-02-14', 55),
(18, 'Perdana Express', 'Ipoh', 'Seremban', 18, '11:00:00', '2021-02-14', 55),
(19, 'Perdana Express', 'Ipoh', 'Seremban', 20, '23:00:00', '2021-02-14', 60),
(20, 'Perdana Express', 'Ipoh', 'Butterworth', 11, '09:15:00', '2021-02-14', 50),
(21, 'Transnasional', 'Shah Alam', 'Stesen Bas Jitra', 7, '14:30:00', '2021-02-14', 55),
(22, 'Transnasional', 'Ipoh', 'Stesen Bas Jitra', 18, '15:00:00', '2021-02-14', 55),
(23, 'Transnational', 'Ipoh', 'Stesen Bas Jitra', 18, '09:00:00', '2021-02-15', 55),
(24, 'Transnational', 'Butterwoth', 'Stesen Bas Jitra', 20, '10:30:00', '2021-02-15', 60),
(25, 'Transnatinal', 'Butterwoth', 'Stesen Bas Jitra', 11, '12:00:00', '2021-02-15', 60),
(26, 'Sani Express', 'Batu Pahat', 'TBS', 35, '09:00:00', '2021-02-15', 55),
(27, 'Sani Express', 'Batu Pahat', 'TBS', 7, '13:00:00', '2021-03-15', 55),
(28, 'Sani Express', 'Kulai', 'TBS', 18, '10:00:00', '2021-03-15', 55),
(29, 'Sani Express', 'Kulai', 'TBS', 20, '18:00:00', '2021-03-15', 55),
(30, 'Sani Express', 'Melaka', 'TBS', 11, '12:00:00', '2021-03-15', 50),
(31, 'Queen Express', 'Kuala Lumpur', 'Kuatan', 35, '11:30:00', '2021-03-15', 55),
(32, 'Queen Express', 'Kuala Lumpur', 'Kuatan', 7, '16:30:00', '2021-03-15', 55),
(33, 'Queen Express', 'Kuantan', 'Raub', 18, '08:30:00', '2021-03-15', 55),
(34, 'Queen Express', 'Kuantan', 'Raub', 20, '14:30:00', '2021-03-14', 50),
(35, 'Queen Express', 'Kuantan', 'Johor Bahru', 11, '12:00:00', '2021-03-15', 50),
(36, 'Perdana Express', 'Alor Setar', 'Ipoh', 35, '20:00:00', '2021-03-14', 55),
(37, 'Perdana Express', 'Teluk Intan', 'Ipoh', 7, '20:15:00', '2021-03-14', 55),
(38, 'Perdana Express', 'Seremban', 'Ipoh', 18, '20:00:00', '2021-03-14', 55),
(39, 'Perdana Express', 'Seremban', 'Ipoh', 20, '08:00:00', '2021-03-15', 60),
(40, 'Perdana Express', 'Butterworth', 'Ipoh', 11, '09:15:00', '2021-03-15', 50);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `FeedbackID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` varchar(50) NOT NULL,
  `Comment` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedback` (`FeedbackID`, `UserID`, `Rating`, `Comment`) VALUES
(1, 1, 'good', 'Excellent service!'),
(2, 2, 'bad', 'Your bus has a bad smell!');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--

CREATE TABLE `user` (
  `UserID` int(11) NOT NULL,
  `Fname` varchar(200) DEFAULT NULL,
  `Lname` varchar(200) DEFAULT NULL,
  `Phone` int(11) DEFAULT NULL,
  `Email` varchar(200) DEFAULT NULL,
  `Username` varchar(200) DEFAULT NULL,
  `Password` varchar(200) DEFAULT NULL,
  `UserType` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user`
--

INSERT INTO `user` (`UserID`, `Fname`, `Lname`, `Email`, `Username`, `Password`, `UserType` , `Phone`) VALUES
(1, 'Mycets', 'Admin', 'admin@mycets.com', 'AdminMy', 'Admin_my', 'admin', 192465342),
(2, 'Alice', 'Wong', 'alice123@gmail.com', 'alice', 'alice1234', 'normal_user', 182435647),
(3, 'Ali', 'Ahmand', 'ali123@gmail.com', 'ali', 'ali1234', 'normal_user', 176435267),
(4, 'SpeedX', 'Admin', 'speedyx123@yahoo.com', 'SpeedX', 'Speed99_', 'admin', 162435647),
(5, 'Alison', 'Wong', 'alison@gmail.com', 'alison', '0987typ', 'normal_user', 154233840),
(6, 'Wilson', 'Sumatharan', 'WilsonS@gmail.com', 'Wilson', '3413569', 'normal_user', 142337494),
(7, 'Elsa', 'Divya', 'DivyaE@gmail.com', 'Divya', '4325689', 'normal_user', 132639406),
(8, 'Quanrine', 'Kong', 'QuanRine@gmail.com', 'QuanRine', '4532897', 'normal_user', 123986452),
(9, 'Daniel', 'Ali', 'DanielAli@gmail.com', 'Daniel', 'da89643', 'normal_user', 112634944),
(10, 'Zarifah', 'Amminudin', 'ZarifahA@gmail.com', 'Zarifah', '2ewrs67', 'normal_user', 198352422),
(11, 'Yeoh', 'Ah Chai', 'YeohAC@gmail.com', 'Yeoh', 'tre6589', 'normal_user', 173425637),
(12, 'Tariq', 'Abu', 'TariqAbu@gmail.com', 'Tariq', '23freot', 'normal_user', 163432587),
(13, 'Rooter', 'Zhang', 'RooterZ@gmail.com', 'Rooter', '67534ew', 'normal_user', 132538364),
(14, 'Candy', 'Crush', 'CCrush@gmail.com', 'Candy', 'C534rdg', 'normal_user', 124364527),
(15, 'Horse', 'Row', 'HorseRow@gmail.com', 'Horse', 'Hg54638', 'normal_user', 117354623),
(16, 'Kelvin', 'Wong', 'KelvinW@gmail.com', 'Kelvin', 'K64539', 'normal_user', 125467322),
(17, 'Jason', 'Alimen', 'AlimenJ@gmail.com', 'Jason', 'Tr54622', 'normal_user', 198253427),
(18, 'Nelene', 'Jenuang', 'NeleneJ@gmail.com', 'Nelene', 'N903624', 'normal_user', 16534299),
(19, 'Susan', 'Tereen', 'SusanT@gmail.com', 'Susan', 'S89655e', 'normal_user', 134255644),
(20, 'Lerene', 'Lee', 'LereneL@gmail.com', 'Lerene', 'L9077353', 'normal_user', 146533422),
(21, 'Opera', 'Ajhad', 'OperaJ@gmail.com', 'Opera', 'O897735', 'normal_user', 165422897);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `booking`
  ADD PRIMARY KEY (`Booking_id`);

--
-- Indexes for table `bus`
--
ALTER TABLE `bus`
  ADD PRIMARY KEY (`Trip_Id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`FeedbackID`);

--
-- Indexes for table `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`UserID`),
  ADD UNIQUE KEY `Username` (`Username`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `booking`
  MODIFY `Booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `bus`
--
ALTER TABLE `bus`
  MODIFY `Trip_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `FeedbackID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--
ALTER TABLE `user`
  MODIFY `UserID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ferryonlineticketingsystem`
--
-- --------------------------------------------------------

--
-- Table structure for table `bookingf`
--

CREATE TABLE `bookingf` (
  `Booking_id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Ferry_id` int(11) NOT NULL,
  `User_id` int(11) DEFAULT NULL,
  `Seats_no` int(11) NOT NULL,
  `Total_fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `bookingf` (`Booking_id`, `user`, `Date`, `Ferry_id`, `User_id`, `Seats_no`, `Total_fare`) VALUES
(1, 'Siti', '2021-06-03 23:34:38', 1, 1, 2, 15),
(2, 'Alice', '2021-06-04 16:03:02', 2, 2, 1, 15),
(3, 'Ali', '2021-06-04 18:52:06', 3, 3, 3, 8),
(4, 'alison', '2021-07-06 11:38:26', 4, 5, 1, 25),
(5, 'Wilson', '2021-07-06 11:39:58', 4, 6, 2, 25),
(6, 'Divya', '2021-06-03 13:34:38', 8, 7, 2, 12),
(7, 'QuanRine', '2021-06-04 09:03:02', 8, 8, 1, 35),
(8, 'Daniel', '2021-06-04 02:52:06', 3, 9, 1, 60),
(9, 'Zarifah', '2021-07-06 01:38:26', 1, 10, 1, 55),
(10, 'Yeoh', '2021-07-06 21:39:58', 26, 11, 2, 35),
(11, 'Tariq', '2021-06-03 21:34:38', 34, 12, 2, 50),
(12, 'Rooter', '2021-06-04 15:03:02', 40, 13, 1, 30),
(13, 'Candy', '2021-06-04 12:52:06', 12, 14, 1, 45),
(14, 'Horse', '2021-07-06 19:38:26', 20, 15, 1, 20),
(15, 'Kelvin', '2021-07-06 17:39:58', 24, 16, 2, 60),
(16, 'Siti', '2020-06-03 23:50:38', 21, 1, 2, 25),
(17, 'Alice', '2020-06-04 16:10:02', 22, 2, 1, 25),
(18, 'QuanRine', '2021-06-04 09:12:06', 29, 8, 1, 55),
(19, 'Daniel', '2020-07-06 22:38:26', 23, 9, 1, 25),
(20, 'Zarifah', '2020-07-06 01:39:58', 21, 10, 6, 15);

-- --------------------------------------------------------

--
-- Table structure for table `ferry`
--

CREATE TABLE `ferry` (
  `Trip_Id` int(11) NOT NULL,
  `Origin` varchar(200) NOT NULL,
  `Destination` varchar(200) NOT NULL,
  `Seats` int(11) NOT NULL,
  `Departure_time` time NOT NULL,
  `Date_` date NOT NULL,
  `Fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `ferry`
--

INSERT INTO `ferry` (`Trip_Id`, `Origin`, `Destination`, `Seats`, `Departure_time`, `Date_`, `Fare`) VALUES
(1, 'Terminal Jeti Kuala Perlis', 'Terminal Jeti Langkawi', 112, '10:30:00', '2021-05-14', 33),
(2, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Perlis', 95, '10:00:00', '2021-05-14', 33),
(3, 'Terminal Jeti Kuala Kedah', 'Terminal Jeti Langkawi', 80, '15:00:00', '2021-05-14', 45),
(4, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Kedah', 85, '10:30:00', '2021-05-14', 45),
(5, 'Terminal Jeti Batu Musang Penang', 'Terminal Jeti Pulau Aman Penang', 30, '16:00:00', '2021-03-14', 15),
(6, 'Terminal Jeti Batu Musang Penang', 'Terminal Jeti Pulau Aman Penang', 25, '09:00:00', '2021-03-14', 8),
(7, 'Terminal Jeti Batu Musang Penang', 'Terminal Jeti Pulau Aman Penang', 25, '13:00:00', '2021-03-14', 8),
(8, 'Terminal Jeti Pulau Aman Penang', 'Terminal Jeti Batu Musang Penang', 25, '10:00:00', '2021-03-14', 8),
(9, 'Terminal Jeti Pulau Aman Penang', 'Terminal Jeti Batu Musang Penang', 25, '18:00:00', '2021-03-14', 8),
(10, 'Terminal Jeti Pulau Aman Penang', 'Terminal Jeti Batu Musang Penang', 25, '12:00:00', '2021-03-14', 8),
(11, 'Terminal Jeti Kuala Kedah', 'Terminal Jeti Langkawi', 65, '11:30:00', '2021-04-12', 45),
(12, 'Terminal Jeti Kuala Kedah', 'Terminal Jeti Langkawi', 87, '16:30:00', '2021-04-13', 45),
(13, 'Terminal Jeti Kuala Kedah', 'Terminal Jeti Langkawi', 68, '08:30:00', '2021-04-14', 45),
(14, 'Terminal Jeti Kuala Perlis', 'Terminal Jeti Langkawi', 80, '14:30:00', '2021-04-15', 33),
(15, 'Terminal Jeti Kuala Perlis', 'Terminal Jeti Langkawi', 71, '12:00:00', '2021-04-18', 33),
(16, 'Terminal Jeti Kuala Perlis', 'Terminal Jeti Langkawi', 75, '09:00:00', '2021-04-18', 33),
(17, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Perlis', 67, '11:15:00', '2020-04-19', 33),
(18, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Perlis', 78, '11:00:00', '2020-04-20', 33),
(19, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Kedah', 70, '23:00:00', '2020-04-21', 45),
(20, 'Terminal Jeti Langkawi', 'Terminal Jeti Kuala Kedah', 80, '09:15:00', '2020-04-22', 45);


-- --------------------------------------------------------

--
-- Table structure for table `feedbackf`
--

CREATE TABLE `feedbackf` (
  `FeedbackID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` varchar(50) NOT NULL,
  `Comment` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedbackf` (`FeedbackID`, `UserID`, `Rating`, `Comment`) VALUES
(1, 1, 'good', 'Excellent service!'),
(2, 2, 'bad', 'Your ferry has a bad smell!');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `bookingf`
  ADD PRIMARY KEY (`Booking_id`);

--
-- Indexes for table `ferry`
--
ALTER TABLE `ferry`
  ADD PRIMARY KEY (`Trip_Id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedbackf`
  ADD PRIMARY KEY (`FeedbackID`);

--
-- Indexes for table `user`
--
--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `bookingf`
  MODIFY `Booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `ferry`
--
ALTER TABLE `ferry`
  MODIFY `Trip_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedbackf`
  MODIFY `FeedbackID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `flightonlineticketingsystem`
--
-- --------------------------------------------------------

--
-- Table structure for table `bookingft`
--

CREATE TABLE `bookingft` (
  `Booking_id` int(11) NOT NULL,
  `user` varchar(200) NOT NULL,
  `Date` timestamp NOT NULL DEFAULT current_timestamp(),
  `Flight_id` int(11) NOT NULL,
  `User_id` int(11) DEFAULT NULL,
  `Seats_no` int(11) NOT NULL,
  `Total_fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `booking`
--

INSERT INTO `bookingft` (`Booking_id`, `user`, `Date`, `Flight_id`, `User_id`, `Seats_no`, `Total_fare`) VALUES
(1, 'Siti', '2021-06-03 23:34:38', 1, 1, 2, 155),
(2, 'Alice', '2021-06-04 16:03:02', 2, 2, 1, 155),
(3, 'Ali', '2021-06-04 18:52:06', 3, 3, 3, 160),
(4, 'alison', '2021-07-06 11:38:26', 4, 5, 1, 55),
(5, 'Wilson', '2020-07-06 11:39:58', 4, 6, 2, 120),
(6, 'Divya', '2021-06-03 13:34:38', 8, 7, 2, 110),
(7, 'QuanRine', '2021-06-04 09:03:02', 8, 8, 1, 155),
(8, 'Daniel', '2021-06-04 02:52:06', 3, 9, 1, 160),
(9, 'Zarifah', '2020-07-06 01:38:26', 1, 10, 1, 155),
(10, 'Yeoh', '2021-07-06 21:39:58', 26, 11, 2, 55),
(11, 'Tariq', '2020-06-03 21:34:38', 34, 12, 2, 150),
(12, 'Rooter', '2021-06-04 15:03:02', 40, 13, 1, 150),
(13, 'Candy', '2020-06-04 12:52:06', 12, 14, 1, 55),
(14, 'Horse', '2021-07-06 19:38:26', 20, 15, 1, 150),
(15, 'Kelvin', '2020-07-06 17:39:58', 24, 16, 2, 160),
(16, 'Siti', '2021-06-03 23:50:38', 21, 1, 2, 55),
(17, 'Alice', '2020-06-04 16:10:02', 22, 2, 1, 155),
(18, 'QuanRine', '2021-06-04 09:12:06', 29, 8, 1, 55),
(19, 'Daniel', '2020-07-06 22:38:26', 23, 9, 1, 155),
(20, 'Zarifah', '2021-07-06 01:39:58', 21, 10, 6, 55);

-- --------------------------------------------------------

--
-- Table structure for table `flight`
--

CREATE TABLE `flight` (
  `Trip_Id` int(11) NOT NULL,
  `Name` varchar(200) NOT NULL,
  `Origin` varchar(200) NOT NULL,
  `Destination` varchar(200) NOT NULL,
  `Seats` int(11) NOT NULL,
  `Departure_time` time NOT NULL,
  `Date_` date NOT NULL,
  `Fare` double NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `flight`
--

INSERT INTO `flight` (`Trip_Id`, `Name`, `Origin`, `Destination`, `Seats`, `Departure_time`, `Date_`, `Fare`) VALUES
(1, 'AirAsia', 'Kota Bharu', 'Johor Bahru', 50, '10:30:00', '2021-02-14', 150),
(2, 'AirAsia', 'Johor Bahru', 'Kota Bharu', 48, '10:00:00', '2021-02-15', 150),
(3, 'MAS', 'Kuala Lumpur', 'Sibu', 67, '15:00:00', '2021-02-15', 230),
(4, 'MAS', 'Sibu', 'Kuala Lumpur', 45, '10:30:00', '2021-02-16', 230),
(5, 'FireFly', 'Kuala Lumpur', 'Johor Bahru', 45, '14:00:00', '2021-02-16', 70),
(6, 'FireFly', 'Johor Bahru', 'Kuala Lumpur', 45, '20:00:00', '2021-02-16', 65),
(7, 'Malindo Air', 'Johor Bahru', 'Pulau Pinang', 35, '13:00:00', '2021-07-19', 85),
(8, 'Malindo Air', 'Pulau Pinang', 'Johor Bahru', 39, '10:00:00', '2021-07-20', 121),
(9, 'AirAsia', 'Pulau Pinang', 'Kota Bharu', 34, '18:00:00', '2021-02-20', 121),
(10, 'AirAsia', 'Kota Bharu', 'Pulau Pinang', 43, '12:00:00', '2021-02-21', 141),
(11, 'MAS', 'Kuantan', 'Kuala Lumpur', 44, '11:30:00', '2021-02-22', 87),
(12, 'MAS', 'Kuantan', 'Kuala Lumpur', 24, '16:30:00', '2021-02-23', 93),
(13, 'FireFly', 'Pulau Pinang', 'Johor Bahru', 30, '08:30:00', '2021-03-01', 123),
(14, 'FireFly', 'Johor Bahru', 'Pulau Pinang', 20, '14:30:00', '2021-03-02', 125),
(15, 'Malindo Air', 'Alor Setar', 'Johor Bahru', 32, '12:00:00', '2021-03-02', 200),
(16, 'Malindo Air', 'Johor Bahru', 'Alor Setar', 35, '09:00:00', '2021-03-03', 195),
(17, 'AirAsia', 'Ipoh', 'Kuala Lumpur', 26, '11:15:00', '2021-03-07', 230),
(18, 'AirAsia', 'Kuala Lumpur', 'Ipoh', 28, '11:00:00', '2021-03-08', 200),
(19, 'MAS', 'Pulau Pinang', 'Seremban', 20, '23:00:00', '2021-03-14', 164),
(20, 'MAS', 'Seremban', 'Pulau Pinang', 30, '09:15:00', '2021-03-15', 158);

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedbackft` (
  `FeedbackID` int(11) NOT NULL,
  `UserID` int(11) NOT NULL,
  `Rating` varchar(50) NOT NULL,
  `Comment` varchar(200) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `feedback`
--

INSERT INTO `feedbackft` (`FeedbackID`, `UserID`, `Rating`, `Comment`) VALUES
(1, 1, 'good', 'Excellent service!'),
(2, 2, 'bad', 'Your flight has a bad smell!');

-- --------------------------------------------------------

--
-- Table structure for table `user`
--


--
-- Indexes for dumped tables
--

--
-- Indexes for table `booking`
--
ALTER TABLE `bookingft`
  ADD PRIMARY KEY (`Booking_id`);

--
-- Indexes for table `flight`
--
ALTER TABLE `flight`
  ADD PRIMARY KEY (`Trip_Id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedbackft`
  ADD PRIMARY KEY (`FeedbackID`);

--
-- Indexes for table `user`
--

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `booking`
--
ALTER TABLE `bookingft`
  MODIFY `Booking_id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT for table `flight`
--
ALTER TABLE `flight`
  MODIFY `Trip_Id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=41;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedbackft`
  MODIFY `FeedbackID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user`
--

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
