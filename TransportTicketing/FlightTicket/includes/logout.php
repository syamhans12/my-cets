<?php session_start(); ?>

<?php
  $_SESSION['s_id'] = null; //destry all user info in session
  $_SESSION['s_firstname'] = null;
  $_SESSION['s_lastname'] = null;
  $_SESSION['s_email'] = null;
  $_SESSION['s_username'] = null;
  $_SESSION['s_password'] = null;
  $_SESSION['s_usertype'] = null;

  session_destroy();

  header("Location: ../index.php");
?>
