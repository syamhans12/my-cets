<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
      <br><h1> Manage User Information </h1><br>
        <div class="manage_table">
          <?php

            if (isset($_GET['source'])) {
                $source = $_GET['source'];
            }
            else {
                $source = "";
            }

            switch ($source) {
                case 'mana_user':
                    include "includes/update_user.php";
                    break;

                default: ?>

          <table class="info_table">
                <thead>
                    <tr>
                        <th>UserId</th>
                        <th>Firstname</th>
                        <th>Lastname</th>
                        <th>PhoneNo</th>
                        <th>Email</th>
                        <th>Username</th>
                        <th>UserType</th>
                    </tr>
                </thead>

                <tbody>

                    <?php


                        $query = "SELECT * FROM user "; //call all info from usertable
                        $choose_user = mysqli_query($connection,$query); //search in database

                        while($row = mysqli_fetch_assoc($choose_user)) {

                          $user_id = $row['UserID'];
                          $user_fname = $row['Fname'];
                          $user_lname = $row['Lname'];
                          $user_phone= $row['Phone'];
                          $user_email= $row['Email'];
                          $username = $row['Username'];
                          $user_type = $row['UserType'];

                     ?>
                    <tr>
                        <td><?php echo $user_id ?></td>
                        <td><?php echo $user_fname ?></td>
                        <td><?php echo $user_lname ?></td>
                        <td><?php echo $user_phone ?></td>
                        <td><?php echo $user_email ?></td>
                        <td><?php echo $username ?></td>
                        <td><?php echo $user_type ?></td>
                        <span>
                        <?php echo "<td><a href='manage_user.php?source=mana_user&user_id=$user_id'>Edit</a></td>"; ?>
                        <?php echo "<td><a href='manage_user.php?delete=$user_id'>Delete</a></td>"; ?>
                        <?php echo "<td><a href='manage_user.php?assign_admin=$user_id'>Assign Admin</a></td>"; ?>
                        <?php echo "<td><a href='manage_user.php?remove_admin=$user_id'>Remove Admin</a></td>"; ?>
                      </span>
                    </tr>
                    <?php } ?>
                </tbody>
              </table>
              <?php
                break;
                }
              ?>

              <?php

              if (isset($_GET['delete'])) {

                  $delete_user_id = $_GET['delete'];
                  $query = "DELETE FROM user WHERE UserID = {$delete_user_id} ";

                  $delete_query = mysqli_query($connection,$query);

                  if(!$delete_query) {
                      die("Query Failed" . mysqli_error($connection));
                  }
                  header("Location: manage_user.php");
                  exit;
              }

              ?>

              <?php

              if (isset($_GET['assign_admin'])) {

                  $user_id = $_GET['assign_admin'];
                  $query = "UPDATE user SET UserType = 'admin' WHERE UserID = '$user_id'";

                  $assign_admin = mysqli_query($connection, $query);

                  if(!$assign_admin) {
                      die("Query Failed" . mysqli_error($connection));
                  }
                  header("Location: manage_user.php");

              }

              ?>

              <?php

              if (isset($_GET['remove_admin'])) {
                  $user_id = $_GET['remove_admin'];
                  $query = "UPDATE user SET UserType = 'normal_user' WHERE UserID = '$user_id'";

                  $remove_admin = mysqli_query($connection, $query);

                  if(!$remove_admin) {
                      die("Query Failed" . mysqli_error($connection));
                  }
                  header("Location: manage_user.php");
              }

              ?>
          </div>
      </div>

<?php include "includes/admin_footer.php"; ?>
