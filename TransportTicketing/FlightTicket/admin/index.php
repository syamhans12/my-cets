<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="center">
    <h1> <i class="fa fa-plane fa-4x" aria-hidden="true"></i>
      <br><br>Welcome to Admin Management <span class="username"><?php
      if(isset($_SESSION['username']))
      echo ucfirst($_SESSION['username']); ?></span> !</h1>
  </div>

<?php include "includes/admin_footer.php"; ?>
