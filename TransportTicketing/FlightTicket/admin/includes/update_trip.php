<?php

  if (isset($_GET['trip_id'])) {
  	$edit_trip_id = $_GET['trip_id'];
  }
  else {
    header("Location: manage_trip.php");
  }

  $query = "SELECT * FROM flight WHERE Trip_Id = $edit_trip_id"; //select from user table where userId is same with
  $select_trip = mysqli_query($connection,$query); //search in database

  while($row = mysqli_fetch_assoc($select_trip)) {

    $edit_trip_id = $row['Trip_Id'];
    $trip_flight_com = $row['Name'];
    $trip_ori = $row['Origin'];
    $trip_dest= $row['Destination'];
    $trip_seat = $row['Seats'];
    $trip_time = $row['Departure_time'];
    $trip_date = $row['Date_'];
    $trip_fare = $row['Fare'];
  }

?>

<div class="info_form">
<form action="" method="Post" enctype="multipart/form-data">

	<div class="update_form">
		<label>FlightCompanyName</label>
		<input type="text" class="form-control" name="t_flightcomname" value="<?php echo $trip_flight_com; ?>" required>
	</div>

  <div class="update_form">
		<label>Origin</label>
		<input type="text" class="form-control" name="t_origin" value="<?php echo $trip_ori; ?>" required>
	</div>

  <div class="update_form">
		<label>Destination</label>
		<input type="text" class="form-control" name="t_dest" value="<?php echo $trip_dest; ?>" required>
	</div>

  <div class="update_form">
		<label>NoOfSeatsAvailable</label>
		<input type="text" class="form-control" name="t_seats" value="<?php echo $trip_seat; ?>" required>
	</div>

  <div class="update_form">
		<label>DepartureTime</label>
		<input type="text" class="form-control" name="t_depart" value="<?php echo $trip_time; ?>" required>
	</div>

  <div class="update_form">
		<label>Date</label><br><br>
		<input type="date" class="form-control" name="t_date" value="<?php echo $trip_date; ?>" required><br><br>
	</div>

  <div class="update_form">
		<label>Fare</label>
		<input type="text" class="form-control" name="t_fare" value="<?php echo $trip_fare; ?>" required>
	</div>

	<div class="update_form">
		<input type="submit" class="btn btn-primary" name="upTrip" value="Update">
	</div>
</form>
</div>

<?php
if (isset($_POST['upTrip'])) {

    $trip_flight_com = $_POST['t_flightcomname'];
    $trip_ori = $_POST['t_origin'];
    $trip_dest= $_POST['t_dest'];
    $trip_seat = $_POST['t_seats'];
    $trip_time = $_POST['t_depart'];
    $trip_date = $_POST['t_date'];
    $trip_fare = $_POST['t_fare'];

  	$query = "UPDATE flight SET Name = '$trip_flight_com', Origin = '$trip_ori', Destination = '$trip_dest', Seats = $trip_seat, Departure_time = '$trip_time', Date_ = '$trip_date', Fare = $trip_fare WHERE Trip_Id = $edit_trip_id ";

  	$update_trip_detail = mysqli_query($connection,$query);
    $source="";

  	if (!$update_trip_detail) {
  		die("Query Failed" . mysqli_error($connection));
	  }

    header("Location: manage_trip.php");
}
?>
