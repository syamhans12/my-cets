<div class="wrapper">
  <div class="top_navbar">
    <div class="bars">
      <div class="bar_one"></div>
      <div class="bar_two"></div>
      <div class="bar_three"></div>
    </div>

    <div class="top_bar">
      <div class="company_title">MYCETS<span class="system_user"> Administrative System</span></div>
        <a href="admin_profile.php">
            <i class="fa fa-user"></i>
            <?php
              if(isset($_SESSION['username']))
              echo ucfirst($_SESSION['username']); ?>
            </a>
    </div>
  </div>

  <div class="side_bar">
    <ul>
      <li><a href="index.php">
        <span class="icon"><i class="fa fa-home fa-lg" aria-hidden="true"></i></span>
        <span class="task">Home</span>
      </a></li>
      <li><a href="manage_user.php">
        <span class="icon"><i class="fa fa-users" aria-hidden="true"></i></span>
        <span class="task">Manage User</span>
      </a></li>
      <li><a href="manage_trip.php">
        <span class="icon"><i class="fa fa-suitcase" aria-hidden="true"></i></span>
        <span class="task">Manage Trip</span>
      </a></li>
      <li><a href="manage_booking.php">
        <span class="icon"><i class="fa fa-briefcase" aria-hidden="true"></i></span>
        <span class="task">Manage Booking</span>
      </a></li>
      <li><a href="view_feedback.php">
        <span class="icon"><i class="fa fa-comments" aria-hidden="true"></i></span>
        <span class="task">View Feedback</span>
      </a></li>
      <div class="logout">
      <li><a href="../includes/logout.php">
        <span class="icon"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i></span>
        <span class="task">Logout</span>
      </a></li>
    </div>
    </ul>
  </div>
