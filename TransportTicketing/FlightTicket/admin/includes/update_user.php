<?php

  if (isset($_GET['user_id'])) {
  	$edit_user_id = $_GET['user_id'];
  }
  else {
    header("Location: manage_user.php");
  }

  $query = "SELECT * FROM user WHERE UserID = $edit_user_id"; //select from user table where userId is same with
  $select_posts = mysqli_query($connection,$query); //search in database

  while($row = mysqli_fetch_assoc($select_posts)) {

      $user_id = $row['UserID']; //store all user info
  		$user_fname = $row['Fname'];
  		$user_lname = $row['Lname'];
      $user_phone = $row['Phone'];
  		$user_email= $row['Email'];
  		$username = $row['Username'];
  }

?>

<div class="info_form">
<form action="" method="Post" enctype="multipart/form-data">

	<div class="update_form">
		<label>Firstname</label>
		<input type="text" class="form-control" name="u_firstname" value="<?php echo $user_fname; ?>" required>
	</div>

	<div class="update_form">
		<label>Lastname</label>
		<input type="text" class="form-control" name="u_lastname" value="<?php echo $user_lname; ?>" required>
	</div>

  <div class="update_form">
		<label>Phone Number</label>
		<input type="text" class="form-control" name="u_phone" value="<?php echo $user_phone; ?>" required>
	</div>

  <div class="update_form">
		<label>Email</label>
		<input type="text" class="form-control" name="u_email" value="<?php echo $user_email; ?>" required>
	</div>

	<div class="update_form">
		<input type="submit" class="btn btn-primary" name="upUser" value="Update">
	</div>
</form>
</div>

<?php
if (isset($_POST['upUser'])) {

  	$user_fname = $_POST['u_firstname'];
  	$user_lname = $_POST['u_lastname'];
    $user_phone = $_POST['u_phone'];
    $user_email = $_POST['u_email'];

  	$query = "UPDATE user SET Fname = '$user_fname', Lname = '$user_lname', Phone = $user_phone,  Email = '$user_email' WHERE UserID = $edit_user_id ";

  	$update_user_detail = mysqli_query($connection,$query);
    $source="";

  	if (!$update_user_detail) {
  		die("Query Failed" . mysqli_error($connection));
	  }

    header("Location: manage_user.php");
}
?>
