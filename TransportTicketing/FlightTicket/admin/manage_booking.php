<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <br><h1> Manage Booking Information </h1><br>
      <div class="manage_table">
        <table class="info_table">
              <thead>
                  <tr>
                      <th>BookingID</th>
                      <th>Username</th>
                      <th>BookingDate</th>
                      <th>TripID</th>
                      <th>UserID</th>
                      <th>NoOfTickets</th>
                      <th>TotalFare</th>
                  </tr>
              </thead>

              <tbody>
                  <?php
                      $query = "SELECT * FROM bookingft "; //call all info from bustable
                      $choose_booking = mysqli_query($connection,$query); //search in database

                      while($row = mysqli_fetch_assoc($choose_booking)) {

                        $book_id = $row['Booking_id'];
                        $book_user = $row['user'];
                        $book_date = $row['Date'];
                        $book_trip = $row['Flight_id'];
                        $book_user_id = $row['User_id'];
                        $book_seat = $row['Seats_no'];
                        $book_fare = $row['Total_fare'];
                   ?>

                  <tr>
                      <td><?php echo $book_id ?></td>
                      <td><?php echo $book_user ?></td>
                      <td><?php echo $book_date ?></td>
                      <td><?php echo $book_trip ?></td>
                      <td><?php echo $book_user_id ?></td>
                      <td><?php echo $book_seat ?></td>
                      <td><?php echo $book_fare ?></td>
                      <span>
                      <?php echo "<td><a href='manage_booking.php?delete=$book_id'>Delete</a></td>"; ?>
                      </span>
                  </tr>
                  <?php } ?>
              </tbody>
            </table>

          <?php

          if (isset($_GET['delete'])) {

              $delete_book_id = $_GET['delete'];
              $query = "DELETE FROM bookingft WHERE Booking_id = {$delete_book_id} ";

              $delete_book_query = mysqli_query($connection,$query);

              if(!$delete_book_query) {
                  die("Query Failed" . mysqli_error($connection));
              }
              header("Location: manage_booking.php");
              exit;
          }

          ?>
        </div>
    </div>

<?php include "includes/admin_footer.php"; ?>
