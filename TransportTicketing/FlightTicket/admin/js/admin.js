$(document).ready(function(){
  $(".bars").click(function(){
     $(".wrapper").toggleClass("collapse");
  });
});

$(function() {
    var CurrentUrl= document.URL;
    var CurrentUrlEnd = CurrentUrl.split('/').filter(Boolean).pop();

    $( ".side_bar ul li a" ).each(function() {
          var ThisUrl = $(this).attr('href');
            var ThisUrlEnd = ThisUrl.split('/').filter(Boolean).pop();
            if(ThisUrlEnd == CurrentUrlEnd)
            $(this).addClass('active')
        });
});
