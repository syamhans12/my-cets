$(document).ready(function(){
  $(".bars").click(function(){
     $(".wrapper").toggleClass("collapse");
  });
});

$(document).ready(function() {
    $('input[type="radio"]').click(function() {
        var inputValue = $(this).attr("value");
        var targetBox = $("." + inputValue);
        $(".pick").not(targetBox).hide();
        $(targetBox).show();
    });
});

function checkDate() {

  if(document.getElementById('roundtrip').checked) {
    trip_type = document.getElementById('roundtrip').value;
  }
  else if(document.getElementById('one-way').checked) {
    trip_type = document.getElementById('one-way').value;
  }

  onDate = document.getElementById("onwardDate").value;
  reDate = document.getElementById("returnDate").value;

  if(trip_type == "roundtrip") {

    if(reDate == "") {
      alert("Please enter the return date!");
      return false;
    }
    else if( onDate < getCurrDate() || reDate < getCurrDate()) {
      alert("The date chosen should not be earlier than today date!");
      return false;
    }
    else if( reDate < onDate ) {
      alert("The return date should not be earlier than the onward date!");
      return false;
    }
  }
  else if(trip_type == "one-way") {
      if( onDate < getCurrDate() ) {
      alert("The date chosen should not be earlier than today date!");
      return false;
    }
  }
}

function getCurrDate() {
  today = new Date();
  year = today.getFullYear();
  month = (today.getMonth()+1);
  day = today.getDate();
  date = year + '-' + ('0' + month).slice(-2) +'-'+ ('0' + day).slice(-2);
  return date;
}

function calcPrice(ticAmt) {
  var amt = ticAmt.value;
  var price = amt * fare;
  document.getElementById("tolPrice").value = price;
}

$(function() {
    var CurrentUrl= document.URL;
    var CurrentUrlEnd = CurrentUrl.split('/').filter(Boolean).pop();

    $( ".side_bar ul li a" ).each(function() {
          var ThisUrl = $(this).attr('href');
            var ThisUrlEnd = ThisUrl.split('/').filter(Boolean).pop();
            if(ThisUrlEnd == CurrentUrlEnd)
            $(this).addClass('active')
        });
});

function validateTextArea() {
  var text = document.getElementById("comments").value;
  if( text.trim().length === 0 ) {
    alert("Please enter comment.");
    return false;
  }
  alert("Thanks for your feedback! We will continue to improve our service.");
}
