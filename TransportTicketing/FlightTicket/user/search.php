<?php include "includes/user_header.php"; ?>
<?php include "includes/user_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <?php
          if (isset($_GET['source'])) {
              $source = $_GET['source'];
          }
          else {
              $source = "";
          }

          switch ($source) {
              case 'book':
                  include "includes/order.php";
                  break;
          }
      ?>
    <?php
    if(isset($_POST['search'])) {
      if($_POST['trip-type'] == "one-way") {
        $source = ucwords($_POST['From']);
        $dest = ucwords($_POST['To']);
        $date = $_POST['onDate'];
        ?>
        <div class="manage_table_two">
        <h2><?php echo ucfirst($source).' -> '.ucfirst($dest).str_repeat('&nbsp;', 5).$date; ?></h2>

            <?php
            if ($source=="" || $dest=="") {
                echo "<h2>*Source And Destination Fields Are Mandatory To Fill</h2>";
            }
            else {
            $query = "SELECT * FROM flight WHERE Origin='$source' AND Destination='$dest' AND Date_='$date' ORDER BY Name";

            $search_query = mysqli_query($connection,$query);

            if(!$search_query) {
                die("Query Failed" . mysqli_error($connection));
            }

            $count_one = mysqli_num_rows($search_query);
            if($count_one == 0) {
                echo "<h1>NO FLIGHT TRIP FOUND</h1>";
            }
            else {

                while($row = mysqli_fetch_assoc($search_query)) {
                    $trip_id = $row['Trip_Id'];
                    $trip_flight_com = $row['Name'];
                    $trip_source = $row['Origin'];
                    $trip_dest = $row['Destination'];
                    $trip_seats = $row['Seats'];
                    $trip_depart = $row['Departure_time'];
                    $trip_date = $row['Date_'];
                    $trip_fare = $row['Fare']
                    ?>

                      <table class="Info Table">
                        <tr>
                          <td>
                            <h4><?php echo $trip_flight_com; ?></h4>
                            <?php ucwords($trip_source)." -> ".ucwords($trip_dest); ?><br>
                            <?php echo $trip_seats; ?> seats <br>
                            <?php echo $trip_date; ?>
                            <?php echo $trip_depart; ?> <br>
                            RM <?php echo $trip_fare; ?>
                          </td>
                          <td>
                              <div class="insert">
                              <?php echo "<a href='search.php?source=book&trip_id=$trip_id'><button>Booking</button></a>"; ?>
                              </div>
                          </td>
                        </tr>
                      </td>
                    </table>
                <?php }
               }
            }
          }
          else if($_POST['trip-type'] == "roundtrip") {
                $on_from = ucwords($_POST['From']);
                $on_to= ucwords($_POST['To']);
                $on_date = $_POST['onDate'];
                $re_from= ucwords($_POST['To']);
                $re_to = ucwords($_POST['From']);
                $re_date = $_POST['reDate'];
            ?>
            <div class="manage_table_two">
            <h2><?php echo ucwords($on_from).' -> '.ucwords($on_to).str_repeat('&nbsp;', 5).$on_date; ?></h2>

                <?php
                if ($on_from=="" || $on_to=="") {
                    echo "<h2>*Source And Destination Fields Are Mandatory To Fill</h2>";
                }
                else {
                $on_query = "SELECT * FROM flight WHERE Origin='$on_from' AND Destination='$on_to' AND Date_='$on_date' ORDER BY Name";

                $on_search_query = mysqli_query($connection,$on_query);

                if(!$on_search_query) {
                    die("Query Failed" . mysqli_error($connection));
                }

                $count_two = mysqli_num_rows($on_search_query);
                if($count_two == 0) {
                    echo "<h1>NO FLIGHT TRIP FOUND</h1>";
                }
                else {

                    while($row = mysqli_fetch_assoc($on_search_query)) {
                        $on_trip_id = $row['Trip_Id'];
                        $on_trip_flight_com = $row['Name'];
                        $on_trip_source = $row['Origin'];
                        $on_trip_dest = $row['Destination'];
                        $on_trip_seats = $row['Seats'];
                        $on_trip_depart = $row['Departure_time'];
                        $on_trip_date = $row['Date_'];
                        $on_trip_fare = $row['Fare']
                        ?>

                          <table class="Info Table">
                            <tr>
                              <td>
                                <h4><?php echo $on_trip_flight_com; ?></h4>
                                <?php ucfirst($on_trip_source)." -> ".ucfirst($on_trip_dest); ?><br>
                                <?php echo $on_trip_seats; ?> seats <br>
                                <?php echo $on_trip_date; ?>
                                <?php echo $on_trip_depart; ?> <br>
                                RM <?php echo $on_trip_fare; ?>
                              </td>
                              <td>
                                  <div class="insert">
                                  <?php echo "<a href='search.php?source=book&trip_id=$on_trip_id'><button>Booking</button></a>"; ?>
                                  </div>
                              </td>
                            </tr>
                          </td>
                        </table>

                    <?php }
                   }
                 }

                ?>

                <br><h2><?php echo ucwords($re_from).' -> '.ucwords($re_to).str_repeat('&nbsp;', 5).$re_date; ?></h2>
                    <?php
                    if ($re_from=="" || $re_to=="") {
                        echo "<h2>*Source And Destination Fields Are Mandatory To Fill</h2>";
                    }
                    else {
                    $re_query = "SELECT * FROM flight WHERE Origin='$re_from' AND Destination='$re_to' AND Date_='$re_date' ORDER BY Name";

                    $re_search_query = mysqli_query($connection,$re_query);

                    if(!$re_search_query) {
                        die("Query Failed" . mysqli_error($connection));
                    }

                    $count_three = mysqli_num_rows($re_search_query);
                    if($count_three == 0) {
                        echo "<br><h1>NO FLIGHT TRIP FOUND</h1>";
                    }
                    else {

                        while($row = mysqli_fetch_assoc($re_search_query)) {
                            $re_trip_id = $row['Trip_Id'];
                            $re_trip_flight_com = $row['Name'];
                            $re_trip_source = $row['Origin'];
                            $re_trip_dest = $row['Destination'];
                            $re_trip_seats = $row['Seats'];
                            $re_trip_depart = $row['Departure_time'];
                            $re_trip_date = $row['Date_'];
                            $re_trip_fare = $row['Fare']
                            ?>

                              <table class="Info Table">
                                <tr>
                                  <td>
                                    <h4><?php echo $re_trip_flight_com; ?></h4>
                                    <?php ucfirst($re_trip_source)." -> ".ucfirst($re_trip_dest); ?><br>
                                    <?php echo $re_trip_seats; ?> seats <br>
                                    <?php echo $re_trip_date; ?>
                                    <?php echo $re_trip_depart; ?> <br>
                                    RM <?php echo $re_trip_fare; ?>
                                  </td>
                                  <td>
                                      <div class="insert">
                                      <?php echo "<a href='search.php?source=book&trip_id=$re_trip_id' ><button>Booking</button></a>"; ?>
                                      </div>
                                  </td>
                                </tr>
                              </td>
                            </table>
                            <?php
                  }
                }
              }
            }
          }
        ?>
          </div>
        </div>

<?php include "includes/user_footer.php" ?>
