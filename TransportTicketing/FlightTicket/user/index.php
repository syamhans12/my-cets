<?php include "includes/user_header.php"; ?>
<?php include "includes/user_navigation.php"; ?>

	<div class="main">
			<div>
	    <h1>Search Trip</h1>
			</div>
			<div id="booking" class="section">
				<div class="section-center">
					<div class="container">
						<div class="row">
							<div id="book_form" class="booking-form">
								<form action="search.php" method="post" id="search-form" onsubmit="return checkDate()">
									<div class="form-group">
										<div class="form-checkbox">
											<label for="roundtrip">
												<input type="radio" id="roundtrip" name="trip-type" value="roundtrip" checked>
												<span></span>Roundtrip
											</label>
											<label for="one-way">
												<input type="radio" id="one-way" name="trip-type" value="one-way">
												<span></span>One way
											</label>
										</div>
									</div>
									<div class="row">
										<div class="col-md-1">
											<div class="form-group">
												<span class="form-label">From</span>
												<input class="form-control" type="text" name="From" placeholder="State or Terminal" required>
											</div>
										</div>
										<div class="col-md-1">
											<div class="form-group">
												<span class="form-label">To</span>
												<input class="form-control" type="text" name="To" placeholder="State or Terminal" required>
											</div>
										</div>
									</div>
									<div class="row">
										<div class="col-md-2">
											<div class="form-group">
												<span class="form-label">Departing</span>
												<input class="form-control" type="date" name="onDate" id=onwardDate required>
											</div>
										</div>
										<div class="col-md-2">
											<div class="form-group">
												<div class="roundtrip pick">
												<span class="form-label">Returning</span>
												<input class="form-control" type="date" name="reDate" id=returnDate>
											  </div>
											</div>
										</div>
										</div>
										<div class="row">
										<div class="col-md-3">
												<div class="form-btn">
													<button class="submit-btn" type="submit" name="search" form="search-form"><i class="fa fa-fw fa-search"></i>Search</button>
												</div>
										</div>
										</div>
								</div>
							</form>
						</div>
					</div>
		</div>
	</div>

<?php include "includes/user_footer.php" ?>
