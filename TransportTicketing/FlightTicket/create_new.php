<!DOCTYPE html>
<html>
<head>
	<title>MYCETS Ticketing System</title>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/LoginStyle.css" rel = "stylesheet" type = "text/css">
</head>
<body>


  <form class="loginform-content animate" action="register.php" method="post">

    <div class="container">

      <label for="uname"><b>Username</b></label>
      <input type="text" placeholder="Enter Username" name="uname" required>

	  <label for="fname"><b>First Name</b></label>
      <input type="text" placeholder="Enter First Name" name="fname" required>

	  <label for="lname"><b>Last Name</b></label>
      <input type="text" placeholder="Enter Last Name" name="lname" required>

	  <label for="phone"><b>Phone Number</b></label>
	  <input type="text" placeholder="Enter Phone Number" name="phone" required>

  	  <label for="email"><b>Email Address</b></label>
	  <input type="text" placeholder="Enter Email Address" name="email" required>

      <label for="psw1"><b>Password</b></label>
      <input type="password" placeholder="Enter Password" name="psw1" required>

	  <label for="psw2"><b>Confirm Password</b></label>
      <input type="password" placeholder="Confirm Password" name="psw2" required>
	  <span id='message'></span>

      <button type="submit">Register <!--****onclick=.....****--> </button>
    </div>

    <div class="container" style="background-color:#f1f1f1">
      <button type="button" onclick="window.location.href='index.php'" class="cancelbtn">Cancel</button>
    </div>
  </form>

</body>
</html>
