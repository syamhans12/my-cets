
<html>

<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/LoginStyle.css" rel = "stylesheet" type = "text/css">
	<title>MYCETS Ticketing System</title>
	<style>
	body {
		margin-top: 115px;
	}

	h3 {
		margin-left: 20px;
	}

	p {
		text-align: center;
		padding: 10px;
		background-color: rgb(230,230,250);
		width: 80%;
	}

	button {
		background-color: #4CAF50;
		color: white;
		padding: 10px;
		margin-left: 20px;
		border: none;
		cursor: pointer;
		width: 8%;
	}
	</style>

</head>

<body>
	<h3>Restore Password Successfully!</h3>
	<div class="container">
		<p>The temporary password has been sent to the registered email address. Please log in to our system for the action of changing new password.</p>
	</div>
	<button onclick="window.location.href='index.php'">Continue</button>

	</form>

</body>
</html>
