<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <br><h1> Manage Trip Information </h1><br>
      <div class="manage_table">
        <?php

          if (isset($_GET['source'])) {
              $source = $_GET['source'];
          }
          else {
              $source = "";
          }

          switch ($source) {
              case 'mana_trip':
                  include "includes/update_trip.php";
                  break;
              case 'add_trip':
                  include "includes/add_trip.php";
                  break;

              default: ?>

        <table class="info_table">
              <thead>
                  <tr>
                      <th>TripId</th>
                      <th>BusCompanyName</th>
                      <th>Origin</th>
                      <th>Destination</th>
                      <th>NoOfSeatsAvailable</th>
                      <th>DepartureTime</th>
                      <th>Date</th>
                      <th>Fare</th>
                  </tr>
              </thead>

              <tbody>

                  <?php


                      $query = "SELECT * FROM bus "; //call all info from bustable
                      $choose_bus = mysqli_query($connection,$query); //search in database

                      while($row = mysqli_fetch_assoc($choose_bus)) {

                        $trip_id = $row['Trip_Id'];
                        $trip_bus_com = $row['Name'];
                        $trip_ori = $row['Origin'];
                        $trip_dest= $row['Destination'];
                        $trip_seat = $row['Seats'];
                        $trip_time = $row['Departure_time'];
                        $trip_date = $row['Date_'];
                        $trip_fare = $row['Fare'];
                   ?>

                  <tr>
                      <td><?php echo $trip_id ?></td>
                      <td><?php echo $trip_bus_com ?></td>
                      <td><?php echo $trip_ori ?></td>
                      <td><?php echo $trip_dest ?></td>
                      <td><?php echo $trip_seat ?></td>
                      <td><?php echo $trip_time ?></td>
                      <td><?php echo $trip_date ?></td>
                      <td><?php echo $trip_fare ?></td>
                      <span>
                      <?php echo "<td><a href='manage_trip.php?source=mana_trip&trip_id=$trip_id'>Edit</a></td>"; ?>
                      <?php echo "<td><a href='manage_trip.php?delete=$trip_id'>Delete</a></td>"; ?>
                    </span>
                  </tr>
                  <?php } ?>
              </tbody>
            </table>
            <br>

          <?php
            break;
            }
          ?>

          <?php

          if (isset($_GET['delete'])) {

              $delete_trip_id = $_GET['delete'];
              $query = "DELETE FROM bus WHERE Id = {$delete_trip_id} ";

              $delete_trip_query = mysqli_query($connection,$query);

              if(!$delete_trip_query) {
                  die("Query Failed" . mysqli_error($connection));
              }
              header("Location: manage_trip.php");
              exit;
          }

          ?>

        </div>
        <div class="insert">
          <?php echo "<a href='manage_trip.php?source=add_trip'><button>Add Trip &nbsp<i class='fa fa-plus-circle' aria-hidden='true'></i></button></a>"; ?>
        </div>
    </div>

<?php include "includes/admin_footer.php"; ?>
