<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
      <br><h1><i class="fa fa-user-circle-o fa-6" aria-hidden="true"></i> <?php
      if(isset($_SESSION['username']))
      echo ucfirst($_SESSION['username']); ?> Profile</h1><br>
          <?php
              $s_sname = $_SESSION['username'];
              $query = "SELECT * FROM user WHERE Username = '$s_sname'" ; //call all info from usertable
              $find_user = mysqli_query($connection,$query); //search in database

              while($row = mysqli_fetch_assoc($find_user)) {

                $user_id = $row['UserID'];
                $user_fname = $row['Fname'];
                $user_lname = $row['Lname'];
                $user_phone= $row['Phone'];
                $user_email= $row['Email'];
                $username = $row['Username'];
                $password = $row['Password'];
                $user_type = $row['UserType'];
              }
           ?>
          <div class="profile_info">
            <div class="profile_update">
                <label>Username : </label><Span><?php echo $username ?></span>
            </div>
            <div class="profile_update">
                <label>FirstName : </label><Span><?php echo $user_fname ?></span>
            </div>
            <div class="profile_update">
                <label>LastName : </label><Span><?php echo $user_lname ?></span>
            </div>
            <div class="profile_update">
                <label>PhoneNo: </label><Span><?php echo $user_phone ?></span>
            </div>
            <div class="profile_update">
                <label>Email : </label><Span><?php echo $user_email ?></span>
            </div>
            <div class="profile_update">
                <label>UserType : </label><Span><?php echo $user_type ?></span>
            </div>
            <div class="insert">
              <a href="update_profile.php"><button>Update &nbsp<i class="fa fa-pencil-square-o" aria-hidden="true"></i></button></a>
              <a href="../includes/logout.php"><button>Logout &nbsp<i class="fa fa-sign-out fa-lg" aria-hidden="true"></i></button></a>
            </div>
      </div>
  </div>

<?php include "includes/admin_footer.php"; ?>
