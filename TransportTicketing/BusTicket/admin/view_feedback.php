<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <br><h1> View Customer Feedback </h1><br>
      <div class="manage_table">
        <table class="info_table">
              <thead>
                  <tr>
                      <th>FeedbackID</th>
                      <th>UserID</th>
                      <th>Rating</th>
                      <th>Comment</th>
                  </tr>
              </thead>

              <tbody>
                  <?php
                      $vf_query = "SELECT * FROM feedback"; //call all info from bustable
                      $view_feedback = mysqli_query($connection,$vf_query); //search in database

                      while($row = mysqli_fetch_assoc($view_feedback)) {
                        $f_id = $row['FeedbackID'];
                        $f_userid = $row['UserID'];
                        $f_rating = $row['Rating'];
                        $f_comments = $row['Comment'];
                   ?>

                  <tr>
                      <td><?php echo $f_id ?></td>
                      <td><?php echo $f_userid ?></td>
                      <td><?php echo $f_rating ?></td>
                      <td><?php echo $f_comments ?></td>
                      <span>
                      <?php echo "<td><a href='view_feedback.php?delete=$f_id'>Delete</a></td>"; ?>
                      </span>
                  </tr>
                  <?php } ?>
              </tbody>
            </table>
            <div class="insert">
            <?php echo "<a href='view_feedback.php?delete_all=1'><button>Delete All <i class='fa fa-trash' aria-hidden='true'></i></button></a>"; ?>
            </div>

          <?php

          if (isset($_GET['delete'])) {

              $delete_feedback_id = $_GET['delete'];
              $df_query = "DELETE FROM feedback WHERE FeedbackID = {$delete_feedback_id} ";

              $delete_fb_query = mysqli_query($connection,$df_query);

              if(!$delete_fb_query) {
                  die("Query Failed" . mysqli_error($connection));
              }
              header("Location: view_feedback.php");
              exit;
          }

          ?>

          <?php

          if (isset($_GET['delete_all'])) {

              $daf_query = "DELETE FROM feedback";

              $delete_afb_query = mysqli_query($connection,$daf_query);
              if(!$delete_afb_query) {
                  die("Query Failed" . mysqli_error($connection));
              }

              header("Location: view_feedback.php");
              exit;
          }

          ?>
        </div>
    </div>

<?php include "includes/admin_footer.php"; ?>
