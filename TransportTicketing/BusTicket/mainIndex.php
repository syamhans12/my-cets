<?php include "includes/database.php" ?>
<?php include "includes/headermain.php" ?>

<body>

	<div id="grid">
		  <?php include "includes/mainnavigation.php" ?>
		</div>
	<div id="main">
		<div id="booking" class="section">
			<div class="section-center">
				 <div class='photo-grid-container'>
					<div class='photo-grid'>
						<div class='photo-grid-item first-item'>
							<button onclick="document.location='index.php'"> BUS TICKETING </button>
						</div>
						<div class='photo-grid-item'>
							<button onclick="document.location='../FerryTicket/index.php'"> FERRY TICKETING </button>
						</div>
						<div class='photo-grid-item last-item'>
							<button onclick="document.location='../FlightTicket/index.php'"> FLIGHT TICKETING </button>
						</div>

						</div>

					</div>
				</div>
		</div>
		</div>

	</body>
<?php include "includes/Mainfooter.php" ?>
