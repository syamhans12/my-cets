<?php include "includes/database.php" ?>
<?php include "includes/header.php" ?>

<style>




.photo-grid-container {
	padding-top: 50px;
  display: flex;
  justify-content: center;
}

.photo-grid {
  width:100px;
  display: flex;
  justify-content: flex-start;
  flex-wrap: wrap;
  justify-content: center;
  flex-direction: row;

}

.photo-grid-item {
  width: 300px;
  height: 150px;
}

.first-item {
  order: 1;
}

.last-item {
  order: -1;
}

button {
		background-color: black;
		border: none;
		color: white;
		padding: 35px 100px;
		text-align: center;
		text-decoration: none;
		font-size: 20px;
		border-radius: 8px;
}



</style>

	<div id="grid">
		  <?php include "includes/mainnavigation.php" ?>
		</div>
	<div id="main">
		<div id="booking" class="section">
			<div class="section-center">
				 <div class='photo-grid-container'>
					<div class='photo-grid'>
						<div class='photo-grid-item first-item'>
							<button onclick="document.location='index.php'"> BUS TICKETING </button>
						</div>
						<div class='photo-grid-item'>
							<button onclick="document.location='../FerryTicket/index.php'"> FERRY TICKETING </button>
						</div>
						<div class='photo-grid-item last-item'>
							<button onclick="document.location='../FlightTicket/index.php'"> FLIGHT TICKETING </button>
						</div>

						</div>

					</div>
				</div>
		</div>
		</div>
<?php include "includes/footer.php" ?>
