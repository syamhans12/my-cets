<!-- To start session & Include all value in head section -->
<?php ob_start(); ?>
<?php session_start(); ?>

<?php include "includes/database.php" ?>

<!DOCTYPE html>
<html lang="en">

<head>
	<title>MYCETS Ticketing System</title>

	<meta charset = "utf-8">
	<meta http-equiv = "X-UA-Compatible" content = "IE=edge">
	<meta name = "viewport" content = "width=device-width, initial-scale=1">

	<link href = "https://fonts.googleapis.com/css?family=PT+Sans:400" rel = "stylesheet">
	<link type = "text/css" rel = "stylesheet" href = "css/stylemain.css" />
	<link type = "text/css" rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

	<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
	<script src="js/busTicketing.js"></script>
</head>

<body>
