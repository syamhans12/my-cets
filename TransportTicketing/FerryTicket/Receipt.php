
<?php include "includes/user_header.php"; ?>


<style>


#invoice{
    padding: 30px;
}

.invoice {
    position: relative;
    background-color: #FFF;
    min-height: 680px;
    padding: 15px
}

.invoice header {
    padding: 10px 0;
    margin-bottom: 20px;
    border-bottom: 1px solid #3989c6
}

.invoice .company-details {
    text-align: right
}

.invoice .company-details .name {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .contacts {
    margin-bottom: 20px
}

.invoice .invoice-to {
    text-align: left
}

.invoice .invoice-to .to {
    margin-top: 0;
    margin-bottom: 0
}

.invoice .invoice-details {
    text-align: right
}

.invoice .invoice-details .invoice-id {
    margin-top: 0;
    color: #3989c6
}

.invoice main {
    padding-bottom: 50px
}

.invoice main .thanks {
    margin-top: -100px;
    font-size: 2em;
    margin-bottom: 50px
}

.invoice main .notices {
    padding-left: 6px;
    border-left: 6px solid #3989c6
}

.invoice main .notices .notice {
    font-size: 1.2em
}

.invoice table {
    width: 100%;
    border-collapse: collapse;
    border-spacing: 0;
    margin-bottom: 20px
}

.invoice table td,.invoice table th {
    padding: 15px;
    background: #eee;
    border-bottom: 1px solid #fff
}

.invoice table th {
    white-space: nowrap;
    font-weight: 400;
    font-size: 16px
}

.invoice table td h3 {
    margin: 0;
    font-weight: 400;
    color: #3989c6;
    font-size: 1.2em
}

.invoice table .qty,.invoice table .total,.invoice table .unit {
    text-align: right;
    font-size: 1.2em
}

.invoice table .no {
    color: #fff;
    font-size: 1.6em;
    background: #3989c6
}

.invoice table .unit {
    background: #ddd
}

.invoice table .total {
    background: #3989c6;
    color: #fff
}

.invoice table tbody tr:last-child td {
    border: none
}

.invoice table tfoot td {
    background: 0 0;
    border-bottom: none;
    white-space: nowrap;
    text-align: right;
    padding: 10px 20px;
    font-size: 1.2em;
    border-top: 1px solid #aaa
}

.invoice table tfoot tr:first-child td {
    border-top: none
}

.invoice table tfoot tr:last-child td {
    color: #3989c6;
    font-size: 1.4em;
    border-top: 1px solid #3989c6
}

.invoice table tfoot tr td:first-child {
    border: none
}

.invoice footer {
    width: 100%;
    text-align: center;
    color: #777;
    border-top: 1px solid #aaa;
    padding: 8px 0
}

@media print {
    .invoice {
        font-size: 11px!important;
        overflow: hidden!important
    }

    .invoice footer {
        position: absolute;
        bottom: 10px;
        page-break-after: always
    }

    .invoice>div:last-child {
        page-break-before: always
    }
}

</style>

<script>

$('#printInvoice').click(function(){
           Popup($('.invoice')[0].outerHTML);
           function Popup(data)
           {
               window.print();
               return true;
           }
       });

</script>

<link href="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/css/bootstrap.min.css" rel="stylesheet" id="bootstrap-css">
<script src="//maxcdn.bootstrapcdn.com/bootstrap/4.1.1/js/bootstrap.min.js"></script>
<script src="//cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
<!------ Include the above in your HEAD tag ---------->

<!--Author      : @arboshiki-->
<div id="invoice">

    <div class="toolbar hidden-print">
        <div class="text-right">
            <button id="printInvoice" class="btn btn-info"><i class="fa fa-print"></i> Print</button>
            <button class="btn btn-info"><i class="fa fa-file-pdf-o"></i> Export as PDF</button>
        </div>
        <hr>
    </div>
    <div class="invoice overflow-auto">
        <div style="min-width: 600px">
            <header>
                <div class="row">
                    <div class="col">
                            <img src="https://masha.ai/images/p/my-furniture-cover.jpg" data-holder-rendered="true" style="width:100px;height:100px;" />
                            </a>
                    </div>
                    <div class="col company-details">
                        <h2 class="name">
                            <a>
                            MYCETS
                            </a>
                        </h2>
                        <div>Kuala Lumpur, Malaysia</div>
                        <div>(123) 456-789</div>
                        <div>mycets@gmail.com</div>
                    </div>
                </div>
            </header>


              <?php

                  $_username = $_SESSION['username'];
                  /*-------------------------- User Table ---------------------------*/
                  $check_id_query = "SELECT * FROM user WHERE Username='$_username'";
                  $check_user = mysqli_query($connection,$check_id_query);

                  if (!$check_user) {
                    die("Query Failed" . mysqli_error($connection));
                    header("Location: index.php");
                  }
                  else{
                    while ($row = mysqli_fetch_assoc($check_user)) {
                      $_user_id = $row['UserID'];
                      $_username = $row['Username'];
                    }
                  }

                  /*-------------------------- Booking Table ---------------------------*/
                  $check_book_query = "SELECT * FROM bookingf WHERE User_id=$_user_id";
                  $check_book = mysqli_query($connection,$check_book_query);

                  if(!$check_book) {
                      die("Query Failed" . mysqli_error($connection));
                  }
                  else{
                    while ($row = mysqli_fetch_assoc($check_book)) {
                      $_ferry_id = $row['Ferry_id'];
                      $_seats_no = $row['Seats_no'];
                      $_total_fare = $row['Total_fare'];

                      $check_trip_query = "SELECT * FROM ferry WHERE Trip_id=$_ferry_id";
                      $check_trip = mysqli_query($connection,$check_trip_query);

                      if(!$check_trip) {
                          die("Query Failed" . mysqli_error($connection));
                      }
                      else{
                        while ($row = mysqli_fetch_assoc($check_trip)) {
                        //  $_ferry_name = $row['Name'];
                          $_ferry_origin = $row['Origin'];
                          $_ferry_dest = $row['Destination'];
                          $_ferry_depart = $row['Departure_time'];
                          $_ferry_date = $row['Date_'];
                          $_ferry_fare = $row['Fare'];
                          }
                        }
                      }
                    }

                ?>

            <main>

                <div class="row contacts">
                    <div class="col invoice-to">
                        <div class="text-gray-light">INVOICE TO:</div>
                        <h2 class="to"> <?php echo $_username; ?> </h2>

                    </div>
                    <div class="col invoice-details">
                        <h1 class="invoice-id">INVOICE 3-2-1</h1>


                    </div>
                </div>
                <table border="0" cellspacing="0" cellpadding="0">
                    <thead>
                        <tr>
                            <th>#</th>
                            <th class="text-left">DESCRIPTION</th>
                            <th class="text-right">Bil</th>
                            <th class="text-right">TOTAL</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td class="no">01</td>
                            <td class="text-left">
                              <h3>

                                <br> <?php echo $_ferry_date?> </br>
                                <br> <?php echo $_ferry_origin?> to <?php echo $_ferry_dest?> </br>
                                <br> <?php echo $_ferry_depart?> </br>
                                </h3>



                            </td>

                            <td class="qty"><?php echo $_seats_no?></td>
                            <td class="total">RM <?php echo $_total_fare?></td>
                        </tr>


                </table>

                <div class="notices">
                    <div>NOTICE:</div>
                    <div class="notice">Show this receipt at the departure to proceed for journey </div>
                </div>

            </main>
            <footer>
                Invoice was created on a computer and is valid without the signature and seal.
                  <div class="thanks">Thank you!</div>
                    <button onclick="document.location='check_booking.php'"> Back </button>
            </footer>
        </div>
        <!--DO NOT DELETE THIS div. IT is responsible for showing footer always at the bottom-->
        <div></div>
    </div>
</div>
