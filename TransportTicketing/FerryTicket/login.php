<?php include "includes/database.php"; ob_start();?>
<?php session_start(); ?>


<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link href="css/LoginStyle.css" rel = "stylesheet" type = "text/css">
	<title>MYCETS Ticketing System</title>
	<style>
	a {color:blue; text-decoration: none;}
	a:hover {text-decoration: underline;}
	</style>
</head>
<body>
<form class="loginform-content animate" action="login.php" method="post">
	<div class="imgcontainer">
		Sign in to MYCETS Ferry Online Ticketing System
	</div>

	<div class="container">

		<label for="uname"><b>Username</b></label>
		<input type="text" placeholder="Enter Username" name="uname" value="<?php if(isset($_COOKIE["username"])) { echo $_COOKIE["username"]; } ?>" required>

		<label for="psw"><b>Password</b></label>
		<input type="password" placeholder="Enter Password" name="psw" value="<?php if(isset($_COOKIE["pass"])) { echo $_COOKIE["pass"]; } ?>" required>

		<button type="submit" name="login">Login</button>
		<label>
			<input type="checkbox" name="remember" value="rmb"> Remember me
		</label>
	</div>

	<div class="container" style="background-color:#f1f1f1">
		<button type="button" onclick="window.location.href='index.php'" class="cancelbtn">Cancel</button>
		<span class="psw"><a href="restorePassword.php">Forgot password</a> | <a href="create_new.php">Register new account</a></span>
	</div>
</form>

<!--after getting value from login form */-->
<?php

if (isset($_POST['login'])) {

	$username = $_POST['uname']; //login username value
	$password = $_POST['psw']; //login password value

	$query = "SELECT * FROM user WHERE Username = '$username'"; //select from user table where username is same with
	$selected_user = mysqli_query($connection,$query); //search in database

	if (!$selected_user) { //if the username not found in database
		die("Query Failed" . mysqli_error($connection));
		print '<p>Wrong username!</p>';
		header("Location: login.php");
	}

	while ($row = mysqli_fetch_assoc($selected_user)) {
		$db_user_id = $row['UserID']; //store all user info
		$db_user_fname = $row['Fname'];
		$db_user_lname = $row['Lname'];
		$db_user_email= $row['Email'];
		$db_username = $row['Username'];
		$db_user_password = $row['Password'];
		$db_user_type = $row['UserType'];

		if($username === $db_username && $password === $db_user_password) {

			$_SESSION['id'] = $db_user_id; //store all user info to session
			$_SESSION['firstname'] = $db_user_firstname;
			$_SESSION['lastname'] = $db_user_lastname;
			$_SESSION['email'] = $db_user_email;
			$_SESSION['username'] = $db_username;
			$_SESSION['password'] = $db_user_password;
			$_SESSION['usertype'] = $db_user_type;

			//set cookie for 7 days
			if(isset($_POST['remember'])) {
				setcookie('username', $username, time()+60*60*7);
				setcookie('pass', $password, time()+60*60*7);
			}

			if ($db_user_type === 'admin') { //if role is admin, enter admin interface
				print '<p>Admin!</p>';
				header("Location: admin/index.php");
				exit;
			}
			else if ($db_user_type === 'normal_user') { //if role is normal user, enter user interface
				print '<p>User!</p>';
				header("Location: user/index.php");
				exit;
			}
		}

		else { //if the password not match
			print '<p>Wrong password!</p>';
			header("Location: login.php");
			exit;
		}
	}
}

?>
