<div class="wrapper">
  <div class="top_navbar">
    <div class="bars">
      <div class="bar_one"></div>
      <div class="bar_two"></div>
      <div class="bar_three"></div>
    </div>

    <div class="top_bar">
      <div class="company_title">MYCETS<span class="system_user"> Online Ticketing System</span></div>
        <a href="user_profile.php">
            <i class="fa fa-user"></i>
            <?php
              if(isset($_SESSION['username']))
              echo ucfirst($_SESSION['username']); ?>
            </a>
    </div>
  </div>

  <div class="side_bar">
    <ul>
      <li><a href="index.php">
        <span class="icon"><i class="fa fa-search fa-lg" aria-hidden="true"></i></span>
        <span class="task">Search</span>
      </a></li>
      <li><a href="check_booking.php">
        <span class="icon"><i class="fa fa-book fa-lg" aria-hidden="true"></i></span>
        <span class="task">Check Booking</span>
      </a></li>
      <li><a href="feedback.php">
        <span class="icon"><i class="fa fa-comments-o fa-lg" aria-hidden="true"></i></span>
        <span class="task">Feedback</span>
      </a></li>
      <div class="logout">
      <li><a href="../includes/logout.php">
        <span class="icon"><i class="fa fa-sign-out fa-lg" aria-hidden="true"></i></span>
        <span class="task">Logout</span>
      </a></li>
    </div>
    </ul>
  </div>
