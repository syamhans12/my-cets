<?php
		$order_trip_id = $_GET['trip_id'];
		$query = "SELECT * FROM ferry WHERE Trip_id=$order_trip_id";

		$search_trip_query = mysqli_query($connection,$query);

		if(!$search_trip_query) {
				die("Query Failed" . mysqli_error($connection));
		}
		else {
			while($row = mysqli_fetch_assoc($search_trip_query)) {
					$trip_id = $row['Trip_Id'];
					//$trip_bus_com = $row['Name'];
					$trip_source = $row['Origin'];
					$trip_dest = $row['Destination'];
					$trip_seats = $row['Seats'];
					$trip_depart = $row['Departure_time'];
					$trip_date = $row['Date_'];
					$trip_fare = $row['Fare'];
				}
		}
?>

<?php

	$_username = $_SESSION['username'];

	$query = "SELECT * FROM user WHERE Username = '$_username'"; //select from user table where username is same with
	$_user = mysqli_query($connection,$query); //search in database

	if (!$_user) { //if the username not found in database
		die("Query Failed" . mysqli_error($connection));
		header("Location: index.php");
	}
 	else{
		while ($row = mysqli_fetch_assoc($_user)) {
			$_user_id = $row['UserID']; //store all user info
			$_user_fname = $row['Fname'];
			$_user_lname = $row['Lname'];
			$_username = $row['Username'];
		}
	}
?>
<script>
	var fare = <?php echo $trip_fare; ?>;
</script>

<div class="manage_table">
<h1>Booking form <?php echo $_SESSION['firstname']; ?></h1>
<div class="info_form">
<form action="" method="Post" enctype="multipart/form-data">
	<div class="update_form">
		<label>User</label>
		<input type="text" class="form-control" name="b_name" value="<?php echo $_user_fname ?> "	required>
	</div>

  <div class="update_form">
		<label>No of Ticket(s)</label>
		<input type="number" class="form-control" name="b_ticketamt" min="1" max=<?php echo $trip_seats ?> onchange="calcPrice(this)" required>
	</div>

  <div class="update_form">
		<label>Total Price (RM)
		<input type="text" class="form-control" name="b_tolPrice" id="tolPrice" readonly>
	</div>

	<<div>

		<?php include "includes/choose_seat.php"; ?>

	</div>


	<div class="update_form">
				<div id="paypal-button"></div>
					<script src="https://www.paypalobjects.com/api/checkout.js"></script>
					<script>
						paypal.Button.render({
							// Configure environment
							env: 'sandbox',
							client:
								{
								sandbox: 'demo_sandbox_client_id',
								production: 'demo_production_client_id'
								},
						 // Customize button (optional)
						   locale: 'en_US',
							 style: {
								 	size: 'responsive',
									color: 'gold',
									shape: 'pill',
											},
						// Set up a payment
						payment: function (data, actions) {
						return actions.payment.create({
						transactions: [{
							amount: {
							total: '0.01',
							currency: 'USD'
							}
							}]
							});
							},
							// Execute the payment
							onAuthorize: function (data, actions) {
								return actions.payment.execute()
								.then(function () {
									// Show a confirmation message to the buyer
									window.alert('Thank you for your purchase!');
								});
							}
						}, '#paypal-button');
					</script>

						<input type="submit" class="btn btn-primary" name="payTicket" value="Pay">
	</div>
</form>
</div>

<?php

if (isset($_POST['payTicket'])) {

		$update_trip_id = $trip_id;
		$update_seat_amt = ($trip_seats - $_POST['b_ticketamt']);

		$update_query = "UPDATE ferry SET Seats=$update_seat_amt WHERE Trip_Id=$update_trip_id ";

		$update_book_detail = mysqli_query($connection,$update_query);

		if (!$update_book_detail) {
			die("Query Failed" . mysqli_error($connection));
		}


		$_name = $_POST['b_name'];
		$_ticketamt = $_POST['b_ticketamt'];
		$_tolprice = $_POST['b_tolPrice'];

		$add_query = "INSERT INTO bookingf SET user='$_name', Ferry_id=$update_trip_id, User_id=$_user_id, Seats_no=$_ticketamt, Total_fare=$_tolprice";

		$add_book_detail = mysqli_query($connection,$add_query);

  	if (!$add_book_detail) {
  		die("Query Failed" . mysqli_error($connection));
	  }

		$source="";

		header("Location: check_booking.php");
}
?>
