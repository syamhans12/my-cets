<?php include "includes/user_header.php"; ?>
<?php include "includes/user_navigation.php"; ?>


<style>

.button {
  background-color: Blue;
  border: none;
  color: white;
  padding: 15px 32px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 16px;
  border-radius: 4px;
}

.button span {
  cursor: pointer;
  display: inline-block;
  position: relative;
  transition: 0.5s;
}

.button span:after {
  content: '\00bb';
  position: absolute;
  opacity: 0;
  top: 0;
  right: -20px;
  transition: 0.5s;
}

.button:hover span {
  padding-right: 25px;
}

.button:hover span:after {
  opacity: 1;
  right: 0;
}

</style>


<div class="main">
  <div class="manage_info">
    <div class="manage_table">
      <h2>Upcoming Trip</h2>
        <?php
            $_username = $_SESSION['username'];
            /*-------------------------- User Table ---------------------------*/
            $check_id_query = "SELECT * FROM user WHERE Username='$_username'";
            $check_user = mysqli_query($connection,$check_id_query);

            if (!$check_user) {
              die("Query Failed" . mysqli_error($connection));
              header("Location: index.php");
            }
            else{
              while ($row = mysqli_fetch_assoc($check_user)) {
                $_user_id = $row['UserID'];
                $_username = $row['Username'];
              }
            }

            /*-------------------------- Booking Table ---------------------------*/
            $check_book_query = "SELECT * FROM bookingf WHERE User_id=$_user_id";
            $check_book = mysqli_query($connection,$check_book_query);

            if(!$check_book) {
                die("Query Failed" . mysqli_error($connection));
            }
            else{
              while ($row = mysqli_fetch_assoc($check_book)) {
                $_ferry_id = $row['Ferry_id'];
                $_seats_no = $row['Seats_no'];
                $_total_fare = $row['Total_fare'];

                $check_trip_query = "SELECT * FROM ferry WHERE Trip_id=$_ferry_id";
                $check_trip = mysqli_query($connection,$check_trip_query);

                if(!$check_trip) {
                    die("Query Failed" . mysqli_error($connection));
                }
                else{
                  while ($row = mysqli_fetch_assoc($check_trip)) {
                  //  $_ferry_name = $row['Name'];
                    $_ferry_origin = $row['Origin'];
                    $_ferry_dest = $row['Destination'];
                    $_ferry_depart = $row['Departure_time'];
                    $_ferry_date = $row['Date_'];
                    $_ferry_fare = $row['Fare'];

                    if($_ferry_date > date("Y-m-d")) {

                    ?>
                      <table class="Info Table">
                       <tr>
                         <td>
                           <b><?//php echo $_ferry_name; ?></b>
                           <?php ucfirst($_ferry_origin)." -> ".ucfirst($_ferry_dest); ?><br>
                           <?php echo $_ferry_date; ?>
                           <?php echo $_ferry_depart; ?> <br>
                         </td>
                         <td>
                           <?php echo $_seats_no; ?> ticket(s) <br>
                            Total Price: RM <?php echo $_total_fare; ?>
                         </td>
                       </tr>
                     </table>
                  <?php
                    }
                  }
                }
              }
            }
          ?>
            <br>
            <h2>Complete Trip</h2>
            <?php
                $_username = $_SESSION['username'];
                /*-------------------------- User Table ---------------------------*/
                $check_id_query = "SELECT * FROM user WHERE Username = '$_username'";
                $check_user = mysqli_query($connection,$check_id_query);

                if (!$check_user) {
                  die("Query Failed" . mysqli_error($connection));
                  header("Location: index.php");
                }
                else{
                  while ($row = mysqli_fetch_assoc($check_user)) {
                    $_user_id = $row['UserID'];
                    $_username = $row['Username'];
                  }
                }

                /*-------------------------- Booking Table ---------------------------*/
                $check_book_query = "SELECT * FROM bookingf WHERE User_id=$_user_id";
                $check_book = mysqli_query($connection,$check_book_query);

                if(!$check_book) {
                    die("Query Failed" . mysqli_error($connection));
                }
                else{
                  while ($row = mysqli_fetch_assoc($check_book)) {
                    $_ferry_id = $row['Ferry_id'];
                    $_seats_no = $row['Seats_no'];
                    $_total_fare = $row['Total_fare'];

                    $check_trip_query = "SELECT * FROM ferry WHERE Trip_id=$_ferry_id";
                    $check_trip = mysqli_query($connection,$check_trip_query);

                    if(!$check_trip) {
                        die("Query Failed" . mysqli_error($connection));
                    }
                    else{
                      while ($row = mysqli_fetch_assoc($check_trip)) {
                        //$_ferry_name = $row['Name'];
                        $_ferry_origin = $row['Origin'];
                        $_ferry_dest = $row['Destination'];
                        $_ferry_depart = $row['Departure_time'];
                        $_ferry_date = $row['Date_'];
                        $_ferry_fare = $row['Fare'];

                        if($_ferry_date < date("Y-m-d")) {
                        ?>
                          <table class="Info Table">
                           <tr>
                             <td>
                               <b><?php echo $_ferry_name; ?></b>
                               <?php ucfirst($_ferry_origin)." -> ".ucfirst($_ferry_dest); ?><br>
                               <?php echo $_ferry_date; ?>
                               <?php echo $_ferry_depart; ?> <br>
                             </td>
                             <td>
                               <?php echo $_seats_no; ?> ticket(s) <br>
                                Total Price: RM <?php echo $_total_fare; ?>
                             </td>
                           </tr>
                         </table>
                      <?php
                        }
                      }
                    }
                  }
                }
              ?>
    </div>
      <button class="button" onclick="document.location='Receipt.php'"><span> Receipt </span></button>
  </div>

<?php include "includes/user_footer.php" ?>
