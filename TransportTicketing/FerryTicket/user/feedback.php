<?php include "includes/user_header.php"; ?>
<?php include "includes/user_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <br><h1> Feedback </h1><br>
      <div class="manage_table">
        <?php
            $s_sname = $_SESSION['username'];
            $u_query = "SELECT * FROM user WHERE Username = '$s_sname'" ; //call all info from usertable
            $curr_user = mysqli_query($connection,$u_query); //search in database

            while($row = mysqli_fetch_assoc($curr_user)) {

              $_user_id = $row['UserID'];
              $username = $row['Username'];
            }
        ?>

        <div class="info_form">
        <form action="" method="Post" enctype="multipart/form-data" onsubmit="return validateTextArea()">
          <div class="update_form">
        		<label>How do you rate you trip with us? <span style="color:red"> *</span></label><br><br>
        		<input type="radio" class="form-control" name="rate" value="bad" required>
            <label for="bad">Bad</label>
            <input type="radio" class="form-control" name="rate" value="average" >
            <label for="Average">Average</label>
            <input type="radio" class="form-control" name="rate" value="good" >
            <label for="Good">Good</label>
        	</div><br><br>

          <div class="update_form">
        		<label>Comments:<span style="color:red"> *</span></label><br>
        		<textarea class="form-control" name="comments" id=comments required></textarea>
        	</div>

          <div class="update_form">
        		<input type="submit" class="btn btn-primary" name="Feedback" value="Submit Feedback">
        	</div>
        </form>
        </div>

        <?php
        if (isset($_POST['Feedback'])) {

            $f_rate = $_POST['rate'];
            $f_comments = $_POST['comments'];

          	$f_query = "INSERT INTO feedbackf SET UserID = $_user_id, Rating = '$f_rate', Comment = '$f_comments'";

          	$insert_feedback = mysqli_query($connection,$f_query);

          	if (!$insert_feedback) {
          		die("Query Failed" . mysqli_error($connection));
        	  }

            header("Location: feedback.php");
        }
        ?>
        </div>
    </div>

<?php include "includes/user_footer.php"; ?>
