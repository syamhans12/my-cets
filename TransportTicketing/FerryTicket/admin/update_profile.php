<?php include "includes/admin_header.php"; ?>
<?php include "includes/admin_navigation.php"; ?>

<div class="main">
  <div class="manage_info">
    <br><h1> Update Profile </h1><br>
      <div class="manage_table">
        <?php
            $s_sname = $_SESSION['username'];
            $query = "SELECT * FROM user WHERE Username = '$s_sname'" ; //call all info from usertable
            $find_user = mysqli_query($connection,$query); //search in database

            while($row = mysqli_fetch_assoc($find_user)) {

              $edit_user_id = $row['UserID'];
              $user_fname = $row['Fname'];
              $user_lname = $row['Lname'];
              $user_phone= $row['Phone'];
              $user_email= $row['Email'];
              $username = $row['Username'];
              $password = $row['Password'];
              $user_type = $row['UserType'];
            }
        ?>

        <div class="info_form">
        <form action="" method="Post" enctype="multipart/form-data">
          <div class="update_form">
        		<label>Username</label>
        		<input type="text" class="form-control" name="u_username" value="<?php echo $username; ?>" >
        	</div>

          <div class="update_form">
        		<label>Password</label>
        		<input type="text" class="form-control" name="u_password" value="<?php echo $password; ?>" >
        	</div>

        	<div class="update_form">
        		<label>Firstname</label>
        		<input type="text" class="form-control" name="u_firstname" value="<?php echo $user_fname; ?>" >
        	</div>

        	<div class="update_form">
        		<label>Lastname</label>
        		<input type="text" class="form-control" name="u_lastname" value="<?php echo $user_lname; ?>">
        	</div>

          <div class="update_form">
        		<label>PhoneNo</label>
        		<input type="text" class="form-control" name="u_email" value="<?php echo $user_phone; ?>">
        	</div>

          <div class="update_form">
        		<label>Email</label>
        		<input type="text" class="form-control" name="u_email" value="<?php echo $user_email; ?>">
        	</div>

        	<div class="update_form">
        		<input type="submit" class="btn btn-primary" name="upUser" value="Update">
        	</div>
        </form>
        </div>

        <?php
        if (isset($_POST['upUser'])) {

            $user_fname = $_POST['u_firstname'];
            $user_lname = $_POST['u_lastname'];
            $user_email= $_POST['u_email'];
            $username = $_POST['u_username'];
            $password = $_POST['u_password'];

          	$query = "UPDATE user SET Fname = '$user_fname', Lname = '$user_lname', Phone = $user_phone, Email = '$user_email', Username = '$username', Password = '$password' WHERE UserID = $edit_user_id ";

          	$update_profile_detail = mysqli_query($connection,$query);

          	if (!$update_profile_detail) {
          		die("Query Failed" . mysqli_error($connection));
        	  }

            header("Location: admin_profile.php");
        }
        ?>
        </div>
    </div>

<?php include "includes/admin_footer.php"; ?>
