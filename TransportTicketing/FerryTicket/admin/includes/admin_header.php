<?php session_start(); ?>
<?php ob_start(); ?>
<?php include "../includes/database.php" ?>

<?php

if (!isset($_SESSION['usertype'])) {
    header("Location: ../index.php");
}
else {
    if ($_SESSION['usertype'] !== 'admin') {
        header("Location: ../index.php");
    }
}

?>

<!DOCTYPE html>
<html lang="en">

<head>
    <title>MYCETS Admin Control</title>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link type = "text/css" rel = "stylesheet" href = "css/admin.css">
    <link type = "text/css" rel = "stylesheet" href = "https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
    <script src = "https://code.jquery.com/jquery-3.4.1.min.js"></script>
	  <script src = "js/admin.js"></script>
</head>

<body>
